<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SectorController extends Controller
{

	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
		$arraySectores = $list_sectores->json();

		return view('sectores.index', compact('arraySectores'));
	}

	public function create()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$idServicio = session()->get('usuarioReg_Servicio');
		$servicio = DB::connection('GeaCorpico')->table('TIPO_EMPRESA')
			->select('TIE_DESCRIPCION as nombre')
			->where('TIE_ID', '=', $idServicio)
			->get();

		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=3&update=2018-01-01");
		$sectores = $list_sectores->json();
		$sectores = $sectores[0];

		return view('sectores.create', compact('sectores'))
			->with('nombreServicio', $servicio[0]->nombre);
	}

	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();
		$ultimoSectorRegistrado = DB::connection('GeaCorpico')->table('SECTOR')->orderBy('SEC_ID', 'desc')->first();
		$idSiguiente = $ultimoSectorRegistrado->SEC_ID + 1;

		$ok = DB::connection('GeaCorpico')->table('SECTOR')->insert(
			[
				'SEC_ID' => $idSiguiente,
				'SEC_ID_SERVICIO' => (int)session()->get('usuarioReg_Servicio'),
				'SEC_DESCRIPCION' => $datos['descripcion'],
				'SEC_FECHA_UPDATE' => date('Y-m-d h:i:s'),

			]
		);

		return redirect()->route('sectores.index')
			->with('success', 'Sector creado correctamente');
	}

	public function show($id)
	{
	}

	public function edit($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
		$sectores = $list_sectores->json();
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();

		$sect = DB::connection('GeaCorpico')
			->table('SECTOR')
			->where('SEC_ID', $id)
			->first();

		if ($rol == 1) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					foreach ($sectores as $sector) {
						if ($sector['SEC_ID'] == $sec) {
							$nombreSector = $sector['SEC_DESCRIPCION'];
						}
					}
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
				}
			}
			$nombreSector = $sect->SEC_DESCRIPCION;
		}

		return view('sectores.edit', compact('sect'))
			->with('id', $id)
			->with('idServicio', $serv)
			->with('nombreServicio', $nombreServicio)
			->with('nombreSector', $nombreSector);
	}

	public function update(Request $request, $idSector)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();
		$ok = DB::connection('GeaCorpico')->table('SECTOR')
			->where('SEC_ID', $idSector)
			->update(
				[
					'SEC_DESCRIPCION' => $datos['descripcion']
				]
			);

		return redirect()->route('sectores.index')
			->with('success', 'Sector actualizado correctamente');
	}

	public function destroy($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$registro = DB::connection('GeaCorpico')->table('MOTIVO_TRABAJO_SECTOR')->where('MTS_ID_SECTOR', $id)->get();
		$ok = true;
		if ($registro->all()) {
			//si tene motivos asociados los elimino
			$ok = DB::connection('GeaCorpico')->table('MOTIVO_TRABAJO_SECTOR')->where('MTS_ID_SECTOR', $id)->delete();
		}
		$ok = DB::connection('GeaCorpico')->table('SECTOR')->where('SEC_ID', $id)->delete();

		if ($ok) {
			return redirect()->route('sectores.index')
				->with('success', 'Sector eliminada exitosamente');
		} else {
			return redirect()->route('sectores.index')
				->with('error', 'Ocurrió un error al intentar eliminar el sector');
		}
		return redirect()->route('sectores.index')
			->with('success', 'Sector eliminado correctamente');
	}
}
