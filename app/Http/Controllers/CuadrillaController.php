<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CuadrillaController extends Controller
{
	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$usuarios = Http::get(config('constants.APP_API_HOST') . "/OPERARIOS/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();

		if ($rol != 1) {
			$cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&update=2018-01-01")->json();

			$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
			$sectores = $list_sectores->json();

			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					foreach ($sectores as $sector) {
						if ($sector['SEC_ID'] == $sec) {
							$nombreSector = $sector['SEC_DESCRIPCION'];
						}
					}
				}
			}
		} else {

			$cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&update=2018-01-01")->json();
			$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
			$sectores = $list_sectores->json();

			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
				}
			}

			$nombreSector = '';
		}

		return view('cuadrillas.index', compact('cuadrillas'))
			->with('nombreSector', $nombreSector)
			->with('nombreServicio', $nombreServicio)
			->with('cuadrillas', $cuadrillas)
			->with('sectores', $sectores)
			->with('operarios', $usuarios)
			->with('servicios', $servicios)
			->with('rol', $rol)
			->with('idServicio', $serv);
	}

	public function create()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&update=2018-01-01")->json();
		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
		$sectores = $list_sectores->json();

		$usuarios = Http::get(config('constants.APP_API_HOST') . "/OPERARIOS/?servicio=$serv&update=2018-01-01")->json();
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();

		if ($rol == 1) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					foreach ($sectores as $sector) {
						if ($sector['SEC_ID'] == $sec) {
							$nombreSector = $sector['SEC_DESCRIPCION'];
						}
					}
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
				}
			}
			$nombreSector = '';
		}

		return view('cuadrillas.create', compact('cuadrillas'))
			->with('operarios', $usuarios)
			->with('servicios', $servicios)
			->with('rol', $rol)
			->with('idServicio', $serv)
			->with('nombreServicio', $nombreServicio)
			->with('nombreSector', $nombreSector)
			->with('idSector', $sec)
			->with('cuadrilla', '')
			->with('sectores', $sectores);
	}

	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();
		$ultimaEmpresaContratistaRegistrada = DB::connection('GeaCorpico')->table('EMPRESA_CONTRATISTA')->orderBy('EML_ID', 'desc')->first();
		$ultimaCuadrillaRegistrada = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')->orderBy('TC_ID', 'desc')->first();
		$idSiguienteCuadrilla = $ultimaCuadrillaRegistrada->TC_ID + 1;
		$idSiguienteEmpresaContratista = $ultimaEmpresaContratistaRegistrada->EML_ID + 1;

		//Primero se guarda la cuadrilla en EMPRESA_CONTRATISTA
		$ok = DB::connection('GeaCorpico')->table('EMPRESA_CONTRATISTA')->insert(
			[
				'EML_ID' => (int)$idSiguienteEmpresaContratista,
				'EML_DESCRIPCION' => $datos['descripcion'],
				'EML_DIRECCION' => "Calle 11 nro 341",
				'EML_TELEFONO' => "355555",
				'EML_TIPO_TOMA' => "A",
				'EML_TIPO_EMPRESA' => (int)session()->get('usuarioReg_Servicio'),
				'EML_ACTIVO' => "S",
				'EML_ID_USER' => session()->get('usuarioReg_userName'),
				'EML_FECHA_UPDATE' => date('Y-m-d h:i:s'),
				'EML_LOTE_REPLICACION' => (int)"0"
			]
		);

		//Luego se inserta en TIPO_CUADRILLA CON EL ID GENERADO EN EMP_CON
		$ok = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')->insert(
			[
				'TC_ID' => $idSiguienteCuadrilla,
				'TC_ID_SERVICIO' => (int)session()->get('usuarioReg_Servicio'),
				'TC_ID_SECTOR' => (int)$datos['sector_id'],
				'TC_DESCRIPCION' => $datos['descripcion'],
				'TC_FECHA_UPDATE' => date('Y-m-d h:i:s'),
				'TC_GEASYS_ID' => $idSiguienteEmpresaContratista
			]
		);

		return redirect()->route('cuadrillas.index')
			->with('success', 'Cuadrilla creada correctamente.');
	}

	public function show($id)
	{
	}

	public function edit($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$cuadrilla = DB::connection('GeaCorpico')
			->table('TIPO_CUADRILLA')
			->where('TC_ID', $id)
			->get();

		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01");
		$sectores = $list_sectores->json();

		$usuarios = Http::get(config('constants.APP_API_HOST') . "/OPERARIOS/?servicio=$serv&update=2018-01-01")->json();
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();

		if ($rol == 1) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					foreach ($sectores as $sector) {
						if ($sector['SEC_ID'] == $sec) {
							$nombreSector = $sector['SEC_DESCRIPCION'];
						}
					}
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
				}
			}
			$nombreSector = '';
		}

		$cuadrilla = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
			->select('TC_ID', 'TC_DESCRIPCION', 'SEC_DESCRIPCION')
			->leftjoin('SECTOR', 'SEC_ID', 'TC_ID_SECTOR')
			->where('TC_ID', $id)
			->first();

		return view('cuadrillas.edit', compact('cuadrilla'))
			->with('operarios', $usuarios)
			->with('servicios', $servicios)
			->with('rol', $rol)
			->with('id', $id)
			->with('idServicio', $serv)
			->with('nombreServicio', $nombreServicio)
			->with('nombreSector', $nombreSector)
			->with('sectores', $sectores);
	}

	public function update(Request $request, $idCuadrilla)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();
		if ($datos['sector_id'] != "#") {
			$ok = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
				->where('TC_ID', $idCuadrilla)
				->update(
					[
						'TC_ID_SECTOR' => (int)$datos['sector_id'],
						'TC_DESCRIPCION' => $datos['descripcion'],
					]
				);
		} else {
			$ok = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
				->where('TC_ID', $idCuadrilla)
				->update(
					[
						'TC_DESCRIPCION' => $datos['descripcion']
					]
				);
		}
		//NECESITO EL ID GEASYS PARA ACTUALIZAR LA TABLA EMPRESA CONTRATISTA -_-
		$cuadrilla = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
			->where('TC_ID', $idCuadrilla)
			->select('TC_GEASYS_ID')->first();

		$ok = DB::connection('GeaCorpico')->table('EMPRESA_CONTRATISTA')
			->where('EML_ID', $cuadrilla->TC_GEASYS_ID)
			->update(
				[
					'EML_DESCRIPCION' => $datos['descripcion'],
					'EML_ID_USER' => session()->get('usuarioReg_userName'),
				]
			);
		return redirect()->route('cuadrillas.index')
			->with('success', 'Cuadrilla modificada exitosamente');
	}

	public function destroy($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$registro = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')->where('TTC_ID_CUADRILLA', $id)->get();
		$ok = true;
		if ($registro->all()) {
			//si tene tipos de trabajo asociados los elimino
			$ok = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')->where('TTC_ID_CUADRILLA', $id)->delete();
		}

		//NECESITO EL ID GEASYS PARA ELIMINAR REGISTRO DE LA TABLA EMPRESA CONTRATISTA -_-
		$cuadrilla = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
			->where('TC_ID', $id)
			->select('TC_GEASYS_ID')->first();

		$ok = DB::connection('GeaCorpico')->table('EMPRESA_CONTRATISTA')
			->where('EML_ID', $cuadrilla->TC_GEASYS_ID)->delete();

			//Elimino cuadrilla de TIPO_CUADRILLA
		$ok = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')->where('TC_ID', $id)->delete();
		if ($ok) {
			return redirect()->route('cuadrillas.index')
				->with('success', 'Cuadrilla eliminada exitosamente');
		} else {
			return redirect()->route('cuadrillas.index')
				->with('error', 'Ocurrió un error al intentar eliminar la cuadrilla');
		}
	}
}
