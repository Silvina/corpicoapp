<?php

namespace App\Http\Controllers;

use App\Models\AsignarTTaCuadrilla;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Cuadrilla;
use App\Models\Sector;
use App\Models\TipoServicio;
use App\Config\constants;
use Illuminate\Http\Request;

class AsignarTTaCuadrillaController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		// $roles[0]   = array('1', 'Admin');
		// $roles[1]   = array('2', 'Supervisor');
		// $roles[2]   = array('3', 'Operario');
		/*
		el usuario logueado pertenece a un sector y 1 servicio, a partir de ahi, obtengo tipo_trabajo_Cuadrilla
		por lo tanto a partir de ahi traigo cuadrillas y tipos de trabajo
		*/
		$list_cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();

		return view('asignar-tt-a-cuadrilla.index', compact('list_cuadrillas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//$cuadrilla = new Cuadrilla();
		return view('cuadrilla.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		// $roles[0]   = array('1', 'Admin');
		// $roles[1]   = array('2', 'Supervisor');
		// $roles[2]   = array('3', 'Operario');

		//	$list_usuarios = Http::get(config('constants.APP_API_HOST')."/OPERARIOS/?servicio=3&sector=5&update=2018-01-01");

		/*
		el usuario logueado pertenece a un sector y 1 servicio, a partir de ahi, obtengo tipo_trabajo_Cuadrilla
		por lo tanto a partir de ahi traigo cuadrillas y tipos de trabajo
		
		*/
		// $list_tt_cuadrilla = Http::get(config('constants.APP_API_HOST') . "/TIPO_TRABAJO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$list_cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		// $list_tt = Http::get(config('constants.APP_API_HOST') . "/TIPO_TRABAJO_OPERATIVO/")->json();

		return view('asignar-tt-a-cuadrilla.index', compact('list_cuadrillas'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\AsignarTTaCuadrilla  $asignarTTaCuadrilla
	 * @return \Illuminate\Http\Response
	 */
	public function show(AsignarTTaCuadrilla $asignarTTaCuadrilla)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\AsignarTTaCuadrilla  $asignarTTaCuadrilla
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AsignarTTaCuadrilla $asignarTTaCuadrilla)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		//si le mando sector en cero, es porqe es admin y trae 
		//solo los que pertenecen al servicio, sin importar el sector
		//Esto no se armo porque no hay nadie con sector cero es algo que quedo
		//sin armar funcionalidad por parte de la app

		if ($sec != 0) {
			//si sector es diff de cero, armo select solo con cuadrilla y traigo el nombre del sector
			$sector = DB::connection('GeaCorpico')->table('SECTOR')
				->where('SEC_ID', $sec)
				->get();
			$sectores = '';
			$cuadrillas = DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
				->where('TC_ID_SERVICIO', $serv)
				->where('TC_ID_SECTOR', $sec)
				->get();
		}

		//$list_tt_cuadrillas_asignadas = Http::get(config('constants.APP_API_HOST') . "/TIPO_TRABAJO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$list_tt_cuadrillas_asignadas = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
			->leftjoin('TIPO_CUADRILLA', 'TTC_ID_CUADRILLA', 'TC_ID')
			->leftjoin('TIPO_TRABAJO_OPERATIVO', 'TIT_ID', 'TTC_ID_TIPO_TRABAJO')
			->select('TIPO_TRABAJO_CUADRILLA.TTC_ID_CUADRILLA', 'TIPO_TRABAJO_OPERATIVO.TIT_DESCRIPCION', 'TIPO_TRABAJO_OPERATIVO.TIT_ID')
			->where('TC_ID', '2')
			->get()->toArray();
		$list_todos_tt = Http::get(config('constants.APP_API_HOST') . "/TIPO_TRABAJO_OPERATIVO/?servicio=$serv&sector=$sec&update=2018-01-01")->json();

		$cuadrillas = $cuadrillas->all();

		return view('asignar-tt-a-cuadrilla.edit', compact('cuadrillas'))
			->with('tts', $list_todos_tt)
			->with('cuadrillas', $cuadrillas)
			->with('sector', $sector)
			->with('sectores', $sectores)
			->with('tt_asignadas', $list_tt_cuadrillas_asignadas);
	}

	public function update(Request $request, $id)
	{
		$datos = $request->all();

		dd($datos);
		//ALMACERAR INFORMACION (ID CUADRILLA Y LOS IDS DE TIPO DE TRABAJO EN TABLA TIPO_TRABAJO_CUADRILLA)
		//  borrar asignacion previa y reinsertar

		$asignacionModificada = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
			->where('TTC_ID_CUADRILLA', '=', (int)$datos['id_cuadrilla'])
			->get();
		//dd($asignacionModificada);

		//			$asignacionModificada = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
		//			->where('TTC_ID_CUADRILLA','=', (int)$datos['id_cuadrilla'])
		//			->delete();

		return redirect()->route('AsignarTTaCuadrilla.index')
			->with('success', 'Usuario actualizado correctamente');
	}

	public function destroy(AsignarTTaCuadrilla $asignarTTaCuadrilla)
	{
		//
	}

	public function getCuadrillas($id_sector)
	{
		return DB::connection('GeaCorpico')->table('TIPO_CUADRILLA')
			->where('TC_ID_SECTOR', $id_sector)
			->get();
	}

	public function cuadrilla_ajax(Request $request)
	{
		$response = array();
		$response['tc_id'] = $request->get('TC_ID');

		$list_tt_cuadrillas_asignadas = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
			->leftjoin('TIPO_CUADRILLA', 'TTC_ID_CUADRILLA', 'TC_ID')
			->leftjoin('TIPO_TRABAJO_OPERATIVO', 'TIT_ID', 'TTC_ID_TIPO_TRABAJO')
			->select('TIPO_TRABAJO_CUADRILLA.TTC_ID_CUADRILLA', 'TIPO_TRABAJO_OPERATIVO.TIT_DESCRIPCION', 'TIPO_TRABAJO_OPERATIVO.TIT_ID', 'TIPO_TRABAJO_OPERATIVO.TIT_ABREVIATURA')
			->where('TC_ID', $response['tc_id'])
			->get()->toArray();

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector');

		//$list_todos_tt = Http::get(config('constants.APP_API_HOST') . "/TIPO_TRABAJO_OPERATIVO/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$list_todos_tt = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_OPERATIVO')
			->leftjoin('TIPO_CUADRILLA', 'TTC_ID_CUADRILLA', 'TC_ID')
			->select('TIPO_TRABAJO_OPERATIVO.TIT_DESCRIPCION', 'TIPO_TRABAJO_OPERATIVO.TIT_ID', 'TIPO_TRABAJO_OPERATIVO.TIT_ABREVIATURA')
			->where('Servicio', $serv)
			->where('Sector', $sec)
			->get()->toArray();

		$response['ttasignados'] = $list_tt_cuadrillas_asignadas;
		$response['ttnoasignados'] = $list_todos_tt;

		return response()->json($response);
	}
}
