<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SessionsController extends Controller
{

	public function create()
	{
		return view('autenticacion.login');
	}

	public function store(Request $request)
	{
		$datos = $request->all();
		$response = Http::asForm()->post("http://172.16.14.28:1052/token", [
			'username' => $datos['usuario'],
			'password' => $datos['password'],
			'grant_type' => "password"
		]);

		$usuarioReg = $response->json();

		if ($usuarioReg && $response->status() == '200') {
			if ($usuarioReg['Rol'] == 'Supervisor' || $usuarioReg['Rol'] == 'Admin') {
				/*Definir*/
				$value = $request->session()->get('Apellido');
				$key = 1;

				session()->put('isLogin', true);
				session()->put('usuarioReg', serialize($usuarioReg));

				session()->put('usuarioReg_access_token',$usuarioReg['access_token']);
				session()->put('usuarioReg_token_type',$usuarioReg['token_type']);
				session()->put('usuarioReg_expires_in',$usuarioReg['expires_in']);
				session()->put('usuarioReg_userName',$usuarioReg['userName']);
				session()->put('usuarioReg_Id',$usuarioReg['Id']);
				session()->put('usuarioReg_Servicio',$usuarioReg['Servicio']);
				session()->put('usuarioReg_Sector',$usuarioReg['Sector']);
				session()->put('usuarioReg_Operario',$usuarioReg['Operario']);
				session()->put('usuarioReg_Nombre',$usuarioReg['Nombre']);
				session()->put('usuarioReg_Apellido',$usuarioReg['Apellido']);
				session()->put('usuarioReg_Rol',$usuarioReg['Rol']);
				session()->put('usuarioReg_Cuadrilla',$usuarioReg['Cuadrilla']);
				session()->put('usuarioReg_TipoCuadrilla',$usuarioReg['TipoCuadrilla']);

				return redirect()->route('usuarios.index');
			}
		} else {
			return $response->json();
		}
	}

	public function logout()
	{
		return ("HOLALALALALAL");
	}
}
