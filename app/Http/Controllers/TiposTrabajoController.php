<?php

namespace App\Http\Controllers;

use App\Models\TiposTrabajo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TiposTrabajoController extends Controller
{
	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajos = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_OPERATIVO')->get();
		return view('tipos-trabajo.index', compact('tiposTrabajos'));
	}

	public function create()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajo = new TiposTrabajo();
		return view('tipos-trabajo.create', compact('tiposTrabajo'));
	}

	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();
		$usuario = auth()->user();
		$tipoTrabajo = new TiposTrabajo();
		if ($datos['permite_eliminar'] == 'on')
			$permiteEliminar = 'S';
		else
			$permiteEliminar = 'N';

		if ($datos['activo'] == 'on')
			$activo = 'S';
		else
			$activo = 'N';

		$tipoTrabajo->descripcion = $datos['descripcion'];
		$tipoTrabajo->abreviatura = $datos['abreviatura'];
		$tipoTrabajo->permite_eliminar = $permiteEliminar;
		$tipoTrabajo->activo = $activo;
		$tipoTrabajo->nombre_usuario = $usuario;

		$ok = $tipoTrabajo->save();

		if ($ok) {
			return redirect()->route('tipos-trabajo.index')
				->with('success', 'Tipo de Trabajo creado correctamente.');
		} else {
			return redirect()->route('tipos-trabajo.index')
				->with('error', 'Error al crear tipo de trabajo.');
		}
	}

	public function show($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajo = TiposTrabajo::find($id);
		return view('tipos-trabajo.show', compact('tiposTrabajo'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajo = TiposTrabajo::find($id);
		return view('tipos-trabajo.edit', compact('tiposTrabajo'));
	}

	public function update(Request $request, TiposTrabajo $tiposTrabajo)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajo->update($request->all());

		return redirect()->route('tipos-trabajo.index')
			->with('success', 'TiposTrabajo updated successfully');
	}

	/**
	 * @param int $id
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function destroy($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$tiposTrabajo = TiposTrabajo::find($id)->delete();

		return redirect()->route('tipos-trabajo.index')
			->with('success', 'TiposTrabajo deleted successfully');
	}
}
