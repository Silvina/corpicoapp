<?php

namespace App\Http\Controllers;

use App\Models\MotivosTrabajo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

/**
 * Class MotivosTrabajoController
 * @package App\Http\Controllers
 */
class MotivosTrabajoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$motivosTrabajos = MotivosTrabajo::paginate();

		return view('motivos-trabajo.index', compact('motivosTrabajos'))
			->with('i', (request()->input('page', 1) - 1) * $motivosTrabajos->perPage());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$motivosTrabajo = new MotivosTrabajo();
		return view('motivos-trabajo.create', compact('motivosTrabajo'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		request()->validate(MotivosTrabajo::$rules);

		$motivosTrabajo = MotivosTrabajo::create($request->all());

		return redirect()->route('motivos-trabajos.index')
			->with('success', 'MotivosTrabajo created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$motivosTrabajo = MotivosTrabajo::find($id);

		return view('motivos-trabajo.show', compact('motivosTrabajo'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$motivosTrabajo = MotivosTrabajo::find($id);
		return view('motivos-trabajo.edit', compact('motivosTrabajo'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  MotivosTrabajo $motivosTrabajo
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, MotivosTrabajo $motivosTrabajo)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		request()->validate(MotivosTrabajo::$rules);
		$motivosTrabajo->update($request->all());

		return redirect()->route('motivos-trabajos.index')
			->with('success', 'MotivosTrabajo updated successfully');
	}

	/**
	 * @param int $id
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function destroy($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$motivosTrabajo = MotivosTrabajo::find($id)->delete();

		return redirect()->route('motivos-trabajos.index')
			->with('success', 'MotivosTrabajo deleted successfully');
	}
}
