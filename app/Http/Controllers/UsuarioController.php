<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
	public function index()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$usuariosGea = DB::connection('GeaCorpico')->table('OPERARIO')->get();
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		//OPERARIOS trae de la BD ASP
		$operarios = DB::table('AspNetUsers')
			->select('AspNetUsers.Id as IdUser', 'AspNetUsers.*', 'AspNetUserRoles.*', 'AspNetRoles.*')
			->where('Servicio', '=', $serv)
			->leftJoin('AspNetUserRoles', 'UserId', '=', 'AspNetUsers.Id')
			->leftJoin('AspNetRoles', 'AspNetRoles.Id', '=', 'AspNetUserRoles.RoleId')
			->get();
		$arrayCuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();

		return view('usuario.index', compact('operarios'))
			->with('cuadrillas', $arrayCuadrillas)
			->with('usuarios', $usuariosGea);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$usuariosGea = DB::connection('GeaCorpico')->table('OPERARIO')->get();
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn

		$operarios = Http::get(config('constants.APP_API_HOST') . "/OPERARIOS/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();
		$sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01")->json();
		$cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/?servicio=$serv&sector=$sec&update=2018-01-01")->json();
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		if ($rol == 2) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					foreach ($sectores as $sector) {
						if ($sector['SEC_ID'] == $sec) {
							$nombreSector = $sector['SEC_DESCRIPCION'];
						}
					}
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
				}
			}

			$nombreSector = '';
		}

		return view('usuario.create')
			->with('usuariosGea', $usuariosGea)
			->with('usuarios', $operarios)
			->with('rol', $rol)
			->with('idServicio', $serv)
			->with('nombreServicio', $nombreServicio)
			->with('nombreSector', $nombreSector)
			->with('idSector', $sec)
			->with('cuadrillas', $cuadrillas)
			->with('sectores', $sectores);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$datos = $request->all();

		if (isset($datos['descripcionGeasys'])) {
			$ultimoUsuarioregistrado = DB::connection('GeaCorpico')->table('OPERARIO')->orderBy('OPE_ID', 'desc')->first();
			$idSiguiente = $ultimoUsuarioregistrado->OPE_ID + 1;

			DB::connection('GeaCorpico')->table('OPERARIO')->insert(
				[
					'OPE_ID' => (string)$idSiguiente,
					'OPE_DESCRIPCION' => $datos['descripcionGeasys'],
					'OPE_CONTRATISTA' => "1",
					'OPE_SUCURSAL' => "1",
					'OPE_ACTIVO' => "S",
					'OPE_ID_USER' => session()->get('usuarioReg_userName'),
					'OPE_FECHA_UPDATE' => now()
				]
			);
			return redirect("/usuarios/create")
				->with('success', 'Operario creado correctamente.');;
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin
		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();

		if ($rol == 2) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$servicio_id = $servicio['TIE_ID'];
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$servicio_id = $servicio['TIE_ID'];
				}
			}
		}

		if (isset($datos['operario'])) {

			$campos = Validator::make($request->all(), [
				'operario_id'    => 'required',
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioOperario' => 'required'
			]);

			//genero clave automatica si asi lo decide el usuario formato="ar1948so"
			if ($datos['chec1'] == 'on') {
				$array = str_split($datos['usuarioOperario']);
				$letra1 = $array[0];
				$letra2 =	substr($datos['usuarioOperario'], -1);
				$letra3 = $array[1];
				$letra4 = substr($datos['usuarioOperario'], -2, 1);

				$passwordOperario = $letra1 . $letra2 . '1948' . $letra3 . $letra4;
				$confirmPasswordOperario = $passwordOperario;
			} else {
				if ($datos['chec1'] == 'off') {
					$passwordOperario = $datos['newPasswordOperario'];
					$confirmPasswordOperario = $datos['confirmNewPasswordOperario'];
				}
			}

			$servicio = (string)$servicio_id;
			//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
			$passIguales = strcmp($passwordOperario, $confirmPasswordOperario);

			if ($campos->fails() || $passIguales !== 0) {
				return redirect()->route('usuarios.index')
					->with('error', 'ERROR al generar un usuario');
			}
			dd($datos);
			$usuario = Http::post(
				config('constants.APP_API_HOST') . "/Account/Register/",
				[
					'Servicio' => $servicio,
					'Sector' => (string)$datos['sector_id_operario'],
					'Operario' => (string)$datos['operario_id'],
					'Cuadrilla' => 0,
					'TipoCuadrilla' => 0,
					'Apellido' => $datos['apellido'],
					'Nombre' => $datos['nombre'],
					'Password' => $passwordOperario,
					'ConfirmPassword' => $confirmPasswordOperario,
					'UserName' => $datos['usuarioOperario']
				]
			);

			// ACA DEBE GUARDARSE TAMBIEN EN TABLA AspNetUserRoles EL ID OPERARIO Y EL ROL
			//Traigo el user agregado para tomar el ID generado e insertarlo en la tabla
			$usuarioAgregado = DB::connection('sqlsrv')->table('AspNetUsers')
				->where('Operario', '=', (string)$datos['operario_id'])
				->where('UserName', $datos['usuarioOperario'])
				->get();

			DB::table('AspNetUserRoles')->insert(
				['UserId' => $usuarioAgregado[0]->Id, 'RoleId' => "3"]
			);
			return redirect()->route('usuarios.index')
				->with('success', 'Usuario creado correctamente');
		}

		if (isset($datos['supervisor'])) {

			$campos = Validator::make($request->all(), [
				'operario_id'    => 'required',
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioSupervisor' => 'required'
			]);

			//genero clave automatica si asi lo decide el usuario formato="ar1948so"
			if ($datos['chec2'] == 'on') {
				$array = str_split($datos['usuarioSupervisor']);
				$letra1 = $array[0];
				$letra2 =	substr($datos['usuarioSupervisor'], -1);
				$letra3 = $array[1];
				$letra4 = substr($datos['usuarioSupervisor'], -2, 1);

				$passwordSupervisor = 's' . $letra2 . '1948' . $letra3 . $letra4;
				$confirmPasswordSupervisor = $passwordSupervisor;
			} else {
				if ($datos['chec2'] == 'off') {
					$passwordSupervisor = $datos['newPasswordSupervisor'];
					$confirmPasswordSupervisor = $datos['confirmNewPasswordSupervisor'];
				}
			}
			$servicio = (string)$servicio_id;

			//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
			$passIguales = strcmp($passwordSupervisor, $confirmPasswordSupervisor);

			if ($campos->fails() || $passIguales !== 0) {
				return redirect()->back()
					->with('error', 'ERROR al actualizar el usuario');
			}

			$usuario = Http::post(
				config('constants.APP_API_HOST') . "/Account/Register/",
				[
					'Servicio' => $servicio,
					'Sector' => 	(string)$datos['sector_id_supervisor'],
					'Operario' => (string)$datos['operario_id'],
					'Cuadrilla' => 0,
					'TipoCuadrilla' => 0,
					'Apellido' => $datos['apellido'],
					'Nombre' => $datos['nombre'],
					'Password' => $passwordSupervisor,
					'ConfirmPassword' => $confirmPasswordSupervisor,
					'UserName' => $datos['usuarioSupervisor']
				]
			);

			// ACA DEBE GUARDARSE TAMBIEN EN TABLA AspNetUserRoles EL ID OPERARIO Y EL ROL
			//Traigo el user agregado para tomar el ID generado e insertarlo en la tabla
			$usuarioAgregado = DB::connection('sqlsrv')->table('AspNetUsers')
				->where('Operario', '=', (string)$datos['operario_id'])
				->where('UserName', $datos['usuarioSupervisor'])
				->get();

			DB::table('AspNetUserRoles')->insert(
				['UserId' => $usuarioAgregado[0]->Id, 'RoleId' => "2"]
			);
		}
		return redirect()->route('usuarios.index')
			->with('success', 'Usuario creado correctamente');
	}

	public function show($id)
	{
		if (!session()->get('usuarioReg_token_type')) {
			return redirect()->route('login.store');
		}

		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$usuario = DB::table('AspNetUsers')
			->where('Id', $id)
			->get();

		//$usuarios = Http::get(config('constants.APP_API_HOST')."/OPERARIOS/?servicio=3&sector=5&update=2018-01-01")->json();
		$list_cuadrillas = Http::get(config('constants.APP_API_HOST') . "/TIPO_CUADRILLA/")->json();
		$list_servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();
		$list_sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01")->json();

		//TRAER DATOS DE ROLES DESDE BD ASPNET

		return view('usuario.index', compact('usuario'))
			->with('cuadrillas', $list_cuadrillas)
			->with('sectores', $list_servicios)
			//->with('roles',$roles)
			->with('servicios', $list_sectores);
	}

	public function edit($id)
	{
		$serv = session()->get('usuarioReg_Servicio'); //Si es supervisor, lo toma desde el login. Si es Admin, sector es = 0
		$sec = session()->get('usuarioReg_Sector'); //Este dato viene a partir del logIn
		$rol = session()->get('usuarioReg_Rol'); //este dato viene a partir del logIn 2-supervisor 1-admin

		$servicios = Http::get(config('constants.APP_API_HOST') . "/TIPO_EMPRESA/?update=2018-01-01")->json();
		$sectores = Http::get(config('constants.APP_API_HOST') . "/SECTORS/?servicio=$serv&update=2018-01-01")->json();

		$usuario = DB::table('AspNetUsers')
			->leftjoin('AspNetUserRoles', 'UserId', '=', 'Id')
			->leftjoin('AspNetRoles', 'AspNetRoles.Id', '=', 'AspNetUserRoles.RoleId')
			->where('AspNetUsers.Id', '=', $id)
			->get();

		$usuario = $usuario[0];

		$usuarioGea = DB::connection('GeaCorpico')->table('OPERARIO')
			->where('OPE_ID', $usuario->Operario)
			->get();

		if ($usuario) {
			//asigno los usuarios a cada variable segun el rol
			$usuarioOpe = DB::table('AspNetUsers')
				->leftJoin('AspNetUserRoles', 'UserId', '=', 'AspNetUsers.Id')
				->where('Operario', $usuario->Operario)
				->where('RoleId', '=', '3')
				->get();

			$usuarioOpe = $usuarioOpe->all();

			$usuarioSup = DB::table('AspNetUsers')
				->leftJoin('AspNetUserRoles', 'UserId', '=', 'AspNetUsers.Id')
				->where('Operario', $usuario->Operario)
				->where('RoleId', '=', '2')
				->get();

			$usuarioSup = $usuarioSup->all();

			//Este embrollo de if es para autocompletar el input de userName y de sector de supervisor y operario. 
			//No se pueden contemplar todos las opciones de userName porque hay supervisores que quizas no empiezan con 's'
			if ($usuarioSup && $usuarioOpe) {
				$nombreUsuarioSup = $usuarioSup[0]->UserName;
				$nombreUsuarioOpe = $usuarioOpe[0]->UserName;
				//traigo ID del sector
				foreach ($sectores as $sector) {
					if ($sector['SEC_ID'] == $usuarioOpe[0]->Sector) {
						$sectorOpe = $sector['SEC_ID'];
						$sectorOpeNombre = $sector['SEC_DESCRIPCION'];
						$sectorSupNombre = '';
					} else {
						$sectorOpe = '';
					}
					if ($sector['SEC_ID'] == $usuarioSup[0]->Sector) {
						$sectorSup = $sector['SEC_ID'];
						$sectorSupNombre = $sector['SEC_DESCRIPCION'];
						$sectorOpeNombre = '';
					} else {
						$sectorSup = '';
					}
				}
			} else if ($usuarioOpe) {

				$nombreUsuarioSup = 's' . $usuarioOpe[0]->UserName;
				$nombreUsuarioOpe = $usuarioOpe[0]->UserName;
				//traigo ID del sector
				foreach ($sectores as $sector) {
					if ($sector['SEC_ID'] == $usuarioOpe[0]->Sector) {
						$sectorOpe = $sector['SEC_ID'];
						$sectorOpeNombre = $sector['SEC_DESCRIPCION'];
						$sectorSupNombre = '';
						$sectorSup = '';
					}
				}
			} else if ($usuarioSup) {
				$array = str_split($usuarioSup[0]->UserName);
				$letra1 = $array[0];
				$nombreUsuarioSup = $usuarioSup[0]->UserName;
				if ($letra1 == 's') {
					$nombreUsuarioOpe = substr($usuarioSup[0]->UserName, 1);
				} else {
					$nombreUsuarioOpe = '';
				}
				//traigo ID del sector
				foreach ($sectores as $sector) {
					if ($sector['SEC_ID'] == $usuarioSup[0]->Sector) {
						$sectorSupNombre = $sector['SEC_DESCRIPCION'];
						$sectorOpeNombre = '';
						$sectorSup = $usuarioSup[0]->Sector;
						$sectorOpe = '';
					}
				}
			} else if (!$usuarioSup && !$usuarioOpe) {
				$nombreUsuarioSup = 's' . $usuario->UserName;
				$nombreUsuarioOpe = $usuario->UserName;
				$sectorOpe = '';
				$sectorSup = '';
				$sectorOpeNombre = '';
				$sectorSupNombre = '';
			} else {
				$nombreUsuarioSup = '';
				$nombreUsuarioOpe = '';
				$sectorOpe = '';
				$sectorSup = '';
				$sectorOpeNombre = '';
				$sectorSupNombre = '';
			}
		}
		$nombre = strtolower($usuario->Nombre);
		$apellido = strtolower($usuario->Apellido);

		//servicio
		if ($rol == 2) {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					$id_servicio = $servicio['TIE_ID'];
				}
			}
		} else {
			foreach ($servicios as $servicio) {
				if ($servicio['TIE_ID'] == $serv) {
					$nombreServicio = $servicio['TIE_DESCRIPCION'];
					$id_servicio = $servicio['TIE_ID'];
				}
			}
		}

		return view('usuario.edit', compact('usuario'))
			->with('sector', $sector)
			->with('sectores', $sectores)
			->with('sectorSup', $sectorSup)
			->with('sectorOpe', $sectorOpe)
			->with('nombreServicio', $nombreServicio)
			->with('rol', $rol)
			->with('usuarioGea', $usuarioGea[0])
			->with('usuarioOpe', $usuarioOpe)
			->with('usuarioSup', $usuarioSup)
			->with('nombreUsuarioOpe', $nombreUsuarioOpe)
			->with('nombreUsuarioSup', $nombreUsuarioSup)
			->with('usuario', $usuario)
			->with('id', $id)
			->with('servicios', $servicios)
			->with('sectorOpeNombre', $sectorOpeNombre)
			->with('id_servicio', $id_servicio)
			->with('sectorSupNombre', $sectorSupNombre);
	}

	public function update(Request $request, $id)
	{
		//edita info de usuario
		$datos = $request->all();
		//traigo el usuario para tomar el nro de operario
		$usuario = DB::table('AspNetUsers')
			->where('Id', '=', $id)
			->first();

		if ($usuario) {
			//asigno los usuarios a cada variable segun el rol para luego actualizar por separado la info
			$usuarioOpe = DB::table('AspNetUsers')
				->leftJoin('AspNetUserRoles', 'UserId', '=', 'AspNetUsers.Id')
				->where('Operario', '=', $usuario->Operario)
				->where('AspNetUserRoles.RoleId', '=', '3')
				->first();

			$usuarioSup = DB::table('AspNetUsers')
				->leftJoin('AspNetUserRoles', 'UserId', '=', 'AspNetUsers.Id')
				->where('Operario', '=', $usuario->Operario)
				->where('AspNetUserRoles.RoleId', '=', '2')
				->first();
		}

		//actualizo la info en comun si es que fue editada y si tiene 2 usuarios 
		if ($usuarioOpe) {
			$campos = Validator::make($request->all(), [
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioOperario' => 'required',
			]);

			if ($campos->fails()) {
				return redirect()->back()
					->with('error', 'ERROR al actualizar el usuario');
			} else {

				$usuarioOpeModificado = DB::table('AspNetUsers')
					->where('Id', '=', $usuarioOpe->Id)
					->update([
						'Apellido' => $datos['apellido'],
						'Nombre' => $datos['nombre'],
						'UserName' => $datos['usuario']
					]);
			}
		} elseif (isset($datos['operario']) && !$usuarioOpe) {

			$campos = Validator::make($request->all(), [
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioOperario' => 'required',
			]);

			if ($campos->fails()) {
				return redirect()->back()
					->with('error', 'ERROR al actualizar el usuario');
			} else {
				//genero clave automatica si asi lo decide el usuario formato="ar1948so"
				if ($datos['chec1'] == 'on') {
					$array = str_split($datos['usuarioOperario']);
					$letra1 = $array[0];
					$letra2 =	substr($datos['usuarioOperario'], -1);
					$letra3 = $array[1];
					$letra4 = substr($datos['usuarioOperario'], -2, 1);

					$passwordOperario = $letra1 . $letra2 . '1948' . $letra3 . $letra4;
					$confirmPasswordOperario = $passwordOperario;
				} else {
					if ($datos['chec1'] == 'off') {
						$passwordOperario = $datos['newPasswordOperario'];
						$confirmPasswordOperario = $datos['confirmNewPasswordOperario'];
					}
				}

				$servicio = (string)$datos['servicio'];
				//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
				$passIguales = strcmp($passwordOperario, $confirmPasswordOperario);

				if ($campos->fails() || $passIguales !== 0) {

					return redirect()->route('usuarios.index')
						->with('error', 'ERROR al generar un usuario');
				} else {
					//	$sector = (string)$sector_id;
					$usuarioOpeNuevo = Http::post(
						config('constants.APP_API_HOST') . "/Account/Register/",
						[
							'Servicio' => $servicio,
							'Sector' => (string)$datos['sector_id_operario'],
							'Operario' => (string)$usuario->Operario,
							'Cuadrilla' => 0,
							'TipoCuadrilla' => 0,
							'Apellido' => $datos['apellido'],
							'Nombre' => $datos['nombre'],
							'Password' => $passwordOperario,
							'ConfirmPassword' => $confirmPasswordOperario,
							'UserName' => $datos['usuarioOperario']

						]
					);
					// ACA DEBE GUARDARSE TAMBIEN EN TABLA AspNetUserRoles EL ID OPERARIO Y EL ROL
					//Traigo el user agregado para tomar el ID generado e insertarlo en la tabla
					$usuarioAgregado = DB::connection('sqlsrv')->table('AspNetUsers')
						->where('Operario', '=', (string) $usuario->Operario)
						->where('UserName', $datos['usuarioOperario'])
						->first();

					DB::table('AspNetUserRoles')->insert(
						['UserId' => $usuarioAgregado->Id, 'RoleId' => "3"]
					);

					return redirect()->route('usuarios.index')
						->with('success', 'Usuario creado correctamente');
				}
			}
		}

		if ($usuarioSup) {
			$campos = Validator::make($request->all(), [
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioSupervisor' => 'required',

			]);

			if ($campos->fails()) {
				return redirect()->back()
					->with('error', 'ERROR al actualizar el usuario');
			} else {

				$usuarioSupModificado = DB::table('AspNetUsers')
					->where('AspNetUsers.Id', '=', $usuarioSup->Id)
					->update([
						'Apellido' => $datos['apellido'],
						'Nombre' => $datos['nombre'],

					]);
			}
		} elseif (isset($datos['supervisor']) && !$usuarioSup) {
			$campos = Validator::make($request->all(), [
				'apellido' => 'required',
				'nombre' => 'required',
				'usuarioSupervisor' => 'required',
			]);

			if ($campos->fails()) {
				return redirect()->back()
					->with('error', 'ERROR al actualizar el usuario');
			} else {
				//genero clave automatica si asi lo decide el usuario formato="ar1948so"
				if ($datos['chec2'] == 'on') {
					$array = str_split($datos['usuarioSupervisor']);
					$letra1 = $array[0];
					$letra2 =	substr($datos['usuarioSupervisor'], -1);
					$letra3 = $array[1];
					$letra4 = substr($datos['usuarioSupervisor'], -2, 1);

					$passwordSupervisor = 's' . $letra2 . '1948' . $letra3 . $letra4;
					//	dd($passwordSupervisor);
					$confirmPasswordSupervisor = $passwordSupervisor;
				} else {
					if ($datos['chec2'] == 'off') {
						$passwordSupervisor = $datos['newPasswordSupervisor'];
						$confirmPasswordSupervisor = $datos['confirmNewPasswordSupervisor'];
					}
				}

				//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
				$passIguales = strcmp($passwordSupervisor, $confirmPasswordSupervisor);

				if ($campos->fails() || $passIguales !== 0) {
					return redirect()->back()
						->with('error', 'ERROR al actualizar el usuario');
				} else {

					$usuarioSupNuevo = Http::post(
						config('constants.APP_API_HOST') . "/Account/Register/",
						[
							'Servicio' => $datos['servicio'],
							'Sector' => 	(string)$datos['sector_id_supervisor'],
							'Operario' => (string)$usuario->Operario,
							'Cuadrilla' => 0,
							'TipoCuadrilla' => 0,
							'Apellido' => $datos['apellido'],
							'Nombre' => $datos['nombre'],
							'Password' => $passwordSupervisor,
							'ConfirmPassword' => $confirmPasswordSupervisor,
							'UserName' => $datos['usuarioSupervisor']

						]
					);

					// ACA DEBE GUARDARSE TAMBIEN EN TABLA AspNetUserRoles EL ID OPERARIO Y EL ROL
					//Traigo el user agregado para tomar el ID generado e insertarlo en la tabla
					$usuarioAgregado = DB::connection('sqlsrv')->table('AspNetUsers')
						->where('Operario', '=', (string)$usuario->Operario)
						->where('UserName', $datos['usuarioSupervisor'])
						->get();

					DB::table('AspNetUserRoles')->insert(
						['UserId' => $usuarioAgregado[0]->Id, 'RoleId' => "2"]
					);
				}
			}
		}

		/*************** REGISTRO DE CLAVE DE USUARIO OPERARIO ***************/
		//chec $operario == on 
		if (isset($operario) && $usuarioOpe) {

			if (isset($datos['usuarioOperario'])) {
				$campos = Validator::make($request->all(), [
					'usuarioOperario' => 'required',
				]);

				if ($campos->fails()) {
					return redirect()->back()
						->with('error', 'ERROR al actualizar usuario Operario');
				} else {
					if (isset($datos['newPasswordOperario']) || $datos['chec1'] == 'on') {
						//cambio de clave
						if (isset($datos['newPasswordOperario'])) {
							$passwordOpe = $datos['newPasswordOperario'];
							$confirmPasswordOpe = $datos['confirmNewPasswordOperario'];
						} else {
							//genero clave automatica si asi lo decide el usuario formato="ar1948so"
							if ($datos['chec1'] == 'on') {
								$array = str_split($datos['usuarioOperario']);
								$letra1 = $array[0];
								$letra2 =	substr($datos['usuarioOperario'], -1);
								$letra3 = $array[1];
								$letra4 = substr($datos['usuarioOperario'], -2, 1);

								$passwordOpe = $letra1 . $letra2 . '1948' . $letra3 . $letra4;
								$confirmPasswordOpe = $passwordOpe;
							}
						}

						if ($passwordOpe && $confirmPasswordOpe) {
							//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
							$passIguales = strcmp($passwordOpe, $confirmPasswordOpe);

							if ($passIguales !== 0) {
								return redirect()->back()
									->with('error', 'ERROR al actualizar la contraseña. Estas no coinciden');
							} else {
								//primero borro la clave existente
								$ok = DB::table('AspNetUsers')->where('id', $id)
									->update(
										[
											'SecurityStamp' => NULL,
											'PasswordHash' => NULL
										]
									);

								//Seteo la clave nueva con SetPassword
								$passActualizada = Http::post(
									config('constants.APP_API_HOST') . "/Account/SetPassword/",
									[
										'UserName' => $datos['usuarioOperario'],
										'NewPassword' => $passwordOpe,
										'ConfirmPassword' => $confirmPasswordOpe,
									]
								);

								$passActualizada->status();
							}
						} else {
							return redirect()->back()
								->with('error', 'Debe llenar todos los campos');
						}
					}
				}
			}
		}
		/*************** FIN USUARIO OPERARIO ***************/

		/*************** REGISTRO DE CLAVE USUARIO SUPERVISOR ***************/
		//chec $supervisor == on 
		if (isset($supervisor) && $usuarioSup) {

			if (isset($datos['usuarioSupervisor'])) {
				$campos = Validator::make($request->all(), [
					'usuarioSupervisor' => 'required',
				]);

				if ($campos->fails()) {
					return redirect()->back()
						->with('error', 'ERROR al actualizar usuario Supervisor');
				} else {
					if (isset($datos['newPasswordSupervisor']) || $datos['chec2'] == 'on') {
						//cambio de clave
						if (isset($datos['newPasswordSupervisor'])) {
							$passwordSup = $datos['newPasswordSupervisor'];
							$confirmPasswordSup = $datos['confirmNewPasswordSupervisor'];
						} else {
							//genero clave automatica si asi lo decide el usuario formato="ar1948so"
							if ($datos['chec1'] == 'on') {
								$array = str_split($datos['usuarioSupervisor']);
								$letra1 = $array[0];
								$letra2 =	substr($datos['usuarioSupervisor'], -1);
								$letra3 = $array[1];
								$letra4 = substr($datos['usuarioSupervisor'], -2, 1);

								$passwordSup = $letra1 . $letra2 . '1948' . $letra3 . $letra4;
								$confirmPasswordSup = $passwordSup;
							}
						}

						if ($passwordSup && $confirmPasswordSup) {
							//controlo que las contraseñas ingresadas sean iguales (-1) si no lo son
							$passIguales = strcmp($passwordSup, $confirmPasswordSup);

							if ($passIguales !== 0) {
								return redirect()->back()
									->with('error', 'ERROR al actualizar la contraseña. Estas no coinciden');
							} else {
								//primero borro la clave existente
								$ok = DB::table('AspNetUsers')->where('id', $id)
									->update(
										[
											'SecurityStamp' => NULL,
											'PasswordHash' => NULL
										]
									);

								//Seteo la clave nueva con SetPassword
								$passActualizada = Http::post(
									config('constants.APP_API_HOST') . "/Account/SetPassword/",
									[
										'UserName' => $datos['usuarioSupervisor'],
										'NewPassword' => $passwordSup,
										'ConfirmPassword' => $confirmPasswordSup,
									]
								);
								$passActualizada->status();
							}
						} else {
							return redirect()->back()
								->with('error', 'Debe llenar todos los campos');
						}
					}
				}
			}
		}
		/*************** FIN USUARIO SUPERVISOR ***************/

		return redirect()->route('usuarios.index')
			->with('success', 'Usuario actualizado correctamente');
	}

	/**
	 * @param int $id
	 * @return \Illuminate\Htstp\RedirectResponse
	 * @throws \Exception
	 */
	public function destroy($id)
	{
		$registro = DB::connection('GeaCorpico')->table('CUADRILLA')->where('CU_OPERARIO', $id)->get();
		$ok = true;

		if ($registro->all()) {
			//si tene cuadrilla asociada elimina registro de cuadrilla primero
			$ok = DB::connection('GeaCorpico')->table('CUADRILLA')->where('CU_OPERARIO', $id)->delete();
		}

		// Luego borra de tabla AspNetUsers y AspNetUserRoles
		$ok = DB::table('AspNetUserRoles')->where('UserId', $id)->delete();
		$ok = DB::table('AspNetUsers')->where('id', $id)->delete();

		if ($ok) {
			return redirect()->route('usuarios.index')
				->with('success', 'Usuario eliminado correctamente');
		} else {
			return redirect()->route('usuarios.index')
				->with('error', 'Ocurrió un error al intentar eliminar el usuario');
		}
	}
}
