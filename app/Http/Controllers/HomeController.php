<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->index();
	}

	public function index()
	{
		if(!session()->get('usuarioReg_token_type')){
			return redirect()->route('login.store');
		}
		
		return view('home');
	}
}
