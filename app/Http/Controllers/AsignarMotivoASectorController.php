<?php

namespace App\Http\Controllers;

use App\Models\AsignarMotivoASector;
use App\Models\AsignarTTaCuadrilla;
use Illuminate\Support\Facades\Http;
use App\Models\Cuadrilla;
use App\Models\Sector;
use App\Models\TipoServicio;
use App\Config\constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AsignarMotivoASectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$roles[0]   = Array('1','Admin');
			$roles[1]   = Array('2','Supervisor');
			$roles[2]   = Array('3','Operario');		

		//	$list_usuarios = Http::get(config('constants.APP_API_HOST')."/OPERARIOS/?servicio=3&sector=5&update=2018-01-01");

			$list_sectores = Http::get(config('constants.APP_API_HOST')."/SECTORS/?servicio=3&sector=5&update=2018-01-01")->json();
			$list_motivo_trabajo_sector = Http::get(config('constants.APP_API_HOST')."/MOTIVO_TRABAJO_SECTOR/?servicio=3&sector=5&update=2018-01-01")->json();
			$list_motivo_trabajo = Http::get(config('constants.APP_API_HOST')."/MOTIVO_TRABAJO_OPERATIVO/?servicio=3&sector=5&update=2018-01-01")->json();


			//CONTROLAR LISTADOS SEGUN QUIEN SE LOGUEA Y A PARTIR DE AHI MOSTRAR EL LISTADO
			//LUEGO DEBERIA SELECCIONAR UNA CUADRILLA... LUEGO MOSTRAR TT ASOCIADOS Y POSIBILIDAD DE AGREGAR TT
			//Y ELIMINAR TT ASIGNADOS
			return view('asignar-tt-a-cuadrilla.edit', compact('list_cuadrillas'))
			->with('list_sectores',$list_sectores)
			->with('list_motivo_trabajo_sector',$list_motivo_trabajo_sector)
			->with('list_motivo_trabajo',$list_motivo_trabajo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AsignarMotivoASector  $asignarMotivoASector
     * @return \Illuminate\Http\Response
     */
    public function show(AsignarMotivoASector $asignarMotivoASector)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AsignarMotivoASector  $asignarMotivoASector
     * @return \Illuminate\Http\Response
     */
    public function edit(AsignarMotivoASector $asignarMotivoASector)
    {
			$roles[0]   = Array('1','Admin');
			$roles[1]   = Array('2','Supervisor');
			$roles[2]   = Array('3','Operario');		

		//	$list_usuarios = Http::get(config('constants.APP_API_HOST')."/OPERARIOS/?servicio=3&sector=5&update=2018-01-01");

			$list_sectores = Http::get(config('constants.APP_API_HOST')."/SECTORS/?servicio=3&sector=5&update=2018-01-01")->json();
			$list_motivo_trabajo_sector = Http::get(config('constants.APP_API_HOST')."/MOTIVO_TRABAJO_SECTOR/?servicio=3&sector=5&update=2018-01-01")->json();
			$list_motivo_trabajo = Http::get(config('constants.APP_API_HOST')."/MOTIVO_TRABAJO_OPERATIVO/?servicio=3&sector=5&update=2018-01-01")->json();


			//CONTROLAR LISTADOS SEGUN QUIEN SE LOGUEA Y A PARTIR DE AHI MOSTRAR EL LISTADO
			//LUEGO DEBERIA SELECCIONAR UNA CUADRILLA... LUEGO MOSTRAR TT ASOCIADOS Y POSIBILIDAD DE AGREGAR TT
			//Y ELIMINAR TT ASIGNADOS
			return view('asignar-tt-a-cuadrilla.edit', compact('list_cuadrillas'))
			->with('list_sectores',$list_sectores)
			->with('list_motivo_trabajo_sector',$list_motivo_trabajo_sector)
			->with('list_motivo_trabajo',$list_motivo_trabajo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AsignarMotivoASector  $asignarMotivoASector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$datos = $request->all();
			
			//ALMACERAR INFORMACION (ID CUADRILLA Y LOS IDS DE TIPO DE TRABAJO EN TABLA TIPO_TRABAJO_CUADRILLA)
			//	request()->validate(Usuario::$rules);
	
				//  borrar asignacion previa y reinsertar
	
				$asignacionModificada = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
				->where('TTC_ID_CUADRILLA','=', (int)$datos['id_cuadrilla'])
				->get();
	
				//dd($asignacionModificada);
	
	//			$asignacionModificada = DB::connection('GeaCorpico')->table('TIPO_TRABAJO_CUADRILLA')
	//			->where('TTC_ID_CUADRILLA','=', (int)$datos['id_cuadrilla'])
	//			->delete();
			
					return redirect()->route('AsignarTTaCuadrilla.index')
							->with('success', 'Usuario actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AsignarMotivoASector  $asignarMotivoASector
     * @return \Illuminate\Http\Response
     */
    public function destroy(AsignarMotivoASector $asignarMotivoASector)
    {
        //
    }
}
