<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TiposTrabajo
 *
 * @property $id
 * @property $descripcion
 * @property $abreviatura
 * @property $servicio_id
 * @property $clasificacion_trabajo
 * @property $permite_eliminar
 * @property $activo
 * @property $usuario_id
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class TiposTrabajo extends Model
{
    
    static $rules = [
		'descripcion' => 'required',
		'servicio_id' => 'required',
		'clasificacion_trabajo' => 'required',
		'permite_eliminar' => 'required',
		'activo' => 'required',
		'nombre_usuario' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['descripcion','abreviatura','servicio_id','clasificacion_trabajo','permite_eliminar','activo','usuario_id'];



}
