<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Usuario
 *
 * @property $id
 * @property $legajo
 * @property $apellido
 * @property $nombre
 * @property $cuadrilla_id
 * @property $operario_id
 * @property $nombre_usuario
 * @property $servicio_id
 * @property $sector_id
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Usuario extends Model
{
    
    static $rules = [
		'legajo' => '',
		'apellido' => 'required',
		'nombre' => 'required',
		'cuadrilla_id' => 'required',
		'operario_id' => '',
		'nombre_usuario' => 'required',
		'servicio_id' => 'required',
		'sector_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['legajo','apellido','nombre','cuadrilla_id','operario_id',
																	'nombre_usuario','servicio_id','sector_id'];


		public function Cuadrilla()
		{
			return	$this->hasOne('App\Models\Cuadrilla');
		}

	


}

