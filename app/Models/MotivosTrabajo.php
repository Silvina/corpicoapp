<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MotivosTrabajo
 *
 * @property $id
 * @property $tipo_trabajo_id
 * @property $descripcion
 * @property $abreviatura
 * @property $dias_resolucion
 * @property $alerta_estado_luego_generar
 * @property $estado_luego_generar
 * @property $solicita_colocacion
 * @property $solicita_retiro
 * @property $solicita_informativo
 * @property $solicita_precinto_hab
 * @property $solicita_precinto_med
 * @property $solicita_instalacion
 * @property $solicita_materiales
 * @property $permite_eliminar
 * @property $procesa_tomaestado
 * @property $activo
 * @property $usuario_id
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class MotivosTrabajo extends Model
{
    
    static $rules = [
		'tipo_trabajo_id' => 'required',
		'descripcion' => 'required',
		'alerta_estado_luego_generar' => 'required',
		'estado_luego_generar' => 'required',
		'solicita_colocacion' => 'required',
		'solicita_retiro' => 'required',
		'solicita_informativo' => 'required',
		'solicita_precinto_hab' => 'required',
		'solicita_precinto_med' => 'required',
		'solicita_instalacion' => 'required',
		'solicita_materiales' => 'required',
		'permite_eliminar' => 'required',
		'procesa_tomaestado' => 'required',
		'activo' => 'required',
		'usuario_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tipo_trabajo_id','descripcion','abreviatura','dias_resolucion','alerta_estado_luego_generar','estado_luego_generar','solicita_colocacion','solicita_retiro','solicita_informativo','solicita_precinto_hab','solicita_precinto_med','solicita_instalacion','solicita_materiales','permite_eliminar','procesa_tomaestado','activo','usuario_id'];



}
