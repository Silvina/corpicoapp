<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cuadrilla
 *
 * @property $id
 * @property $tipo_cuadrilla_id
 * @property $operario_id
 * @property $fecha
 * @property $servicio_id
 * @property $sector_id
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cuadrilla extends Model
{
    
    static $rules = [
		'tipo_cuadrilla_id' => 'required',
		'operario_id' => 'required',
		'fecha' => 'required',
		'servicio_id' => 'required',
		'sector_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tipo_cuadrilla_id','operario_id','fecha','servicio_id','sector_id'];

		public function usuario()
			{
				return $this->belongsTo('App\Models\Usuario');
			}
	

}


