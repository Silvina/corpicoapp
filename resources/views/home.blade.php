<!DOCTYPE html>

@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
<h1>HOME</h1>
@stop

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __('Home') }}</div>

				<div class="card-body">

					<div class="alert alert-success" role="alert">
						PRUEBA
					</div>

					{{ __('HOLA!') }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
	console.log('Hi!');
</script>
@stop