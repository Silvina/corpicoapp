@extends('adminlte::page')

@section('title', 'Servicio')

@section('content_header')
<h1>Servicio</h1>
@stop

@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-default">
				<div class="card-header">
					<span class="card-title">Nuevo Servicio</span>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('tipo-servicios.store') }}" role="form" enctype="multipart/form-data">
						@csrf

						@include('tipo-servicio.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection