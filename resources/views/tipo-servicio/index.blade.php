@extends('adminlte::page')

@section('title', 'Servicios')

@section('content_header')
<h1>Servicios</h1>

@stop

@section('content')
<div class="container-fluid">
	@if (session('success'))
	<div class="alert alert-success" role="success">
		{{ session('success') }}
	</div>
	@endif
	@if (session('error'))
	<div class="alert alert-error" role="error">
		{{ session('error') }}
	</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">

						<span id="card_title">
							{{ __('Tipo Servicio') }}
						</span>

						<div class="float-right">
							<a href="{{ route('tipo-servicios.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Nuevo Servicio') }}
							</a>
						</div>
					</div>
				</div>
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
				@endif

				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead class="thead">
								<tr>
									<th>No</th>

									<th>Descripcion</th>

									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($tipoServicios as $tipoServicio)
								<tr>
									<td>{{ ++$i }}</td>

									<td>{{ $tipoServicio->descripcion }}</td>

									<td>
										<form action="{{ route('tipo-servicios.destroy',$tipoServicio->id) }}" method="POST">
											<a class="btn btn-sm btn-primary " href="{{ route('tipo-servicios.show',$tipoServicio->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
											<a class="btn btn-sm btn-success" href="{{ route('tipo-servicios.edit',$tipoServicio->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			{!! $tipoServicios->links() !!}
		</div>
	</div>
</div>
@endsection