@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
@php( $def_container_class = 'container' )
@else
@php( $def_container_class = 'container-fluid' )
@endif
<?php

use Illuminate\Support\Facades\DB;

$idServicio = session()->get('usuarioReg_Servicio');
$servicio = DB::connection('GeaCorpico')->table('TIPO_EMPRESA')
	->select('TIE_DESCRIPCION as nombre')
	->where('TIE_ID', '=', $idServicio)
	->get();

$idSector = session()->get('usuarioReg_Sector');
$sector = DB::connection('GeaCorpico')->table('SECTOR')
	->where('SEC_ID', '=', $idSector)
	->get();
?>

{{-- Default Content Wrapper --}}
<div class="content-wrapper {{ config('adminlte.classes_content_wrapper', '') }}">
	<div style="text-align: right" class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
		{{session()->get('usuarioReg_Nombre')}}
		{{session()->get('usuarioReg_Apellido')}} -
		{{session()->get('usuarioReg_Rol')}} -
		{{$servicio[0]->nombre}}
	</div>
	{{-- Content Header --}}
	@hasSection('content_header')
	<div class="content-header">
		<div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
			@yield('content_header')
		</div>
	</div>
	@endif

	{{-- Main Content --}}
	<!-- <div class="content">
		<div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">


			<hr>
			datos de la session:
			<br>
			{{session()->get('isLogin')}}
			<br>
			{{session()->get('usuarioReg_access_token')}}
			<br>
			{{session()->get('usuarioReg_token_type')}}
			<br>
			{{session()->get('usuarioReg_expires_in')}}
			<br>
			{{session()->get('usuarioReg_userName')}}
			<br>
			{{session()->get('usuarioReg_Id')}}
			<br>
			{{session()->get('usuarioReg_Servicio')}}
			<br>
			{{session()->get('usuarioReg_Sector')}}
			<br>
			{{session()->get('usuarioReg_Operario')}}
			<br>
			{{session()->get('usuarioReg_Nombre')}}
			<br>
			{{session()->get('usuarioReg_Apellido')}}
			<br>
			{{session()->get('usuarioReg_Rol')}}
			<br>
			{{session()->get('usuarioReg_Cuadrilla')}}
			<br>
			{{session()->get('usuarioReg_TipoCuadrilla')}}

			<hr> -->


	@yield('content')
</div>
</div>

</div>