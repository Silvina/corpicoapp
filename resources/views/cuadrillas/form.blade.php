<div class="box box-info padding-1">
	<div class="box-body row">
		<div class="box-body col-md-6">

			<div class="form-group col-md-12">
				{{ Form::label('Servicio: ') }}<br>
				{{$nombreServicio}}
			</div>

			<div class="form-group col-md-12">

				{{ Form::label('Sector:') }}
				@if($cuadrilla!='')
					{{$cuadrilla->SEC_DESCRIPCION}}
					@endif
				<select name='sector_id' id='sector_id' class="form-control js-example-basic-single">
					<option value="#">Seleccione Sector</option>
					@foreach($sectores as $sector)
					<option value="{{$sector['SEC_ID']}}">{{$sector['SEC_DESCRIPCION']}}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-12">
				{{ Form::label('Descripción') }}
				{{ Form::text('descripcion', $cuadrilla=='' ? null : $cuadrilla->TC_DESCRIPCION, ['class' => 'form-control' . ($errors->has('descripcion') ? ' is-invalid' : ''), 'placeholder' => 'Descripción']) }}
				{!! $errors->first('descripcion', '<div class="invalid-feedback">:message</div>') !!}
			</div>
		</div>

	</div>
	<div class="box-footer mt20">
		<button type="submit" class="btn btn-primary">Guardar</button>
	</div>
</div>