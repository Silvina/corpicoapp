@extends('adminlte::page')

@section('title', 'Cuadrillas')

@section('content_header')
<h1>Cuadrillas</h1>
@stop


@section('content')
<div class="container-fluid">
	@if (session('success'))
	<div class="alert alert-success" role="success">
		{{ session('success') }}
	</div>
	@endif
	@if (session('error'))
	<div class="alert alert-error" role="error">
		{{ session('error') }}
	</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<span id="card_title">
							{{ __('Lista de Cuadrillas') }}
						</span>
						<div class="float-right">
							<a href="{{ route('cuadrillas.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Nueva Cuadrilla') }}
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="cuadrillas" class="table table-striped table-hover">
								<thead class="thead">
									<tr>

										<th>Cuadrilla</th>
										@if($nombreSector=='')
										<th>Sector</th>
										@endif
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($cuadrillas as $cuadrilla)

									<tr>
										@if($nombreSector=='')
										@foreach($sectores as $sector)
										@if($sector['SEC_ID']==$cuadrilla['TC_ID_SECTOR'])
										<td>{{ $cuadrilla["TC_DESCRIPCION"] }} </td>
										<td>{{ $sector["SEC_DESCRIPCION"] }}</td>
										<td>
											<form action="{{ route('cuadrillas.destroy',$cuadrilla['TC_ID']) }}" method="POST">
												<a class="btn btn-sm btn-success" href="{{ route('cuadrillas.edit',$cuadrilla['TC_ID']) }}"><i class="fa fa-fw fa-edit"></i></a>
												@csrf
												@method('DELETE')
												<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>
											</form>
										</td>
										@endif
										@endforeach
										@else
										@foreach($sectores as $sector)
										@if($sector['SEC_ID']==$cuadrilla['TC_ID_SECTOR'])
										<td>{{ $cuadrilla["TC_DESCRIPCION"] }} </td>
										<td>
											<form action="{{ route('cuadrillas.destroy',$cuadrilla['TC_ID']) }}" method="POST">
												<a class="btn btn-sm btn-success" href="{{ route('cuadrillas.edit',$cuadrilla['TC_ID']) }}"><i class="fa fa-fw fa-edit"></i></a>
												@csrf
												@method('DELETE')
												<button type="submit" class="btn btn-danger btn-sm  borrar"><i class="fa fa-fw fa-trash"></i></button>
											</form>
										</td>
										@endif
										@endforeach
										@endif

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	@endsection

	@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
	@endsection
	@section('js')
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

	<script>
		$(document).ready(function() {
			$('#cuadrillas').DataTable({
				"language": {
					"search": "Buscar",
					"lengthMenu": "Mostrar _MENU_ registros por página",
					"info": "Mostrando página _PAGE_ de _PAGES_",
					"paginate": {
						"previous": "Anterior",
						"next": "Siguiente",
						"first": "Primero",
						"last": "Ultimo"

					}
				}
			});
		});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

	<script type="text/javascript">
		$('.borrar').click(function(event) {
			var form = $(this).closest("form");
			var name = $(this).data("name");
			event.preventDefault();
			swal({
					title: `Eliminar Cuadrilla`,
					text: "Esta cuadrilla puede tener tareas asignadas. DESEA CONTINUAR?",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})

				.then((willDelete) => {
					if (willDelete) {
						form.submit();
					}
				});
		});
	</script>
	@endsection