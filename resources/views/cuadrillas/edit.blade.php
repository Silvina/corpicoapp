@extends('adminlte::page')

@section('title', 'Modificar Cuadrilla')

@section('content_header')
<h1>Modificar Cuadrilla</h1>
@stop

@section('content')
<section class="content container-fluid">
	<div class="">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-default">
				<div class="card-header">
					<span class="card-title">Modificar Cuadrilla</span>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('cuadrillas.update', $id) }}" role="form" enctype="multipart/form-data">
						{{ method_field('PATCH') }}
						@csrf

						@include('cuadrillas.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection