@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <h1>HOME</h1>
@stop

@section('content')
    <p>Panel Home</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop