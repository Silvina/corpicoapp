@extends('adminlte::page')

@section('title', 'Asignar TT a Cuadrilla')

@section('content_header')
<h1>Asignar TT a Cuadrilla</h1>
@stop


@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">

						<span id="card_title">
							{{ __('Lista') }}
						</span>

						<div class="float-right">
							<a href="{{ route('asignar-tt-a-cuadrilla.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Nueva Cuadrilla') }}
							</a>
						</div>
					</div>
				</div>
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
				@endif

				<div class="card-body">
					<div class="table-responsive">
						<table id="cuadrillas" class="table table-striped table-hover">
							<thead class="thead">
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($list_cuadrillas as $cuadrilla)
								<tr>
									<td>{{ $cuadrilla["TC_DESCRIPCION"] }} </td>
									<td>{{ $cuadrilla["TC_ID_SECTOR"] }} </td>
									<td>{{ $cuadrilla["TC_ID_SERVICIO"] }} </td>
									<td>
										<form action="{{ route('cuadrillas.destroy',$cuadrilla['TC_ID']) }}" method="POST">
											<a class="btn btn-sm btn-success" href="{{ route('cuadrillas.edit',$cuadrilla['TC_ID']) }}"><i class="fa fa-fw fa-edit"></i></a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
	$(document).ready(function() {
		$('#cuadrillas').DataTable({
			"language": {
				"search": "Buscar",
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"paginate": {
					"previous": "Anterior",
					"next": "Siguiente",
					"first": "Primero",
					"last": "Ultimo"
				}
			}
		});
	});
</script>
@endsection