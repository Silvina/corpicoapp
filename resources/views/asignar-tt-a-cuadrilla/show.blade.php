@extends('adminlte::page')

@section('title', 'Cuadrilla')

@section('content_header')
    <h1>Ver Cuadrilla</h1>
@stop

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title"></span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('asignar-tt-a-cuadrilla.index') }}"> Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Cuadrilla:</strong>
                            {{ $cuadrilla->descripcion }}
                        </div>
                        <div class="form-group">
                            <strong>Operario Id:</strong>
                            {{ $cuadrilla->operario_id }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha:</strong>
                            {{ date('d-m-Y',strtotime($cuadrilla->fecha)) }}
                        </div>
                        <div class="form-group">
                            <strong>Servicio Id:</strong>
                            {{ $cuadrilla->servicio_id }}
                        </div>
                        <div class="form-group">
                            <strong>Sector Id:</strong>
                            {{ $cuadrilla->sector_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
