@extends('adminlte::page')

@section('title', 'Asignar TT a Cuadrilla')
@section('plugins.Select2',true)

@section('content_header')
<h1>Asignar TT a Cuadrilla</h1>
@stop

@section('content')
<section class="content container-fluid">
	<div class="">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-body">
				<div class="card-header col-md-12">
					<span class="card-title col-md-12">Pasos a seguir: </span>
					<ol>
						<li> Seleccione una <strong>Cuadrilla</strong> </li>
						<li> Seleccione los <strong>Tipos de Trabajo</strong> asociados a la cuadrilla
							<ul>
								<li> Un clic en el tipo de trabajo de la ventana izquierda para asignar</li>
								<li> Un clic en el tipo de trabajo de la ventana derecha para borrar de la asignación</li>
							</ul>
						</li>
					</ol>

				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('asignar-tt-a-cuadrilla.update', 1) }}" role="form" enctype="multipart/form-data">
						{{ method_field('PATCH') }}
						@csrf

						@include('asignar-tt-a-cuadrilla.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection