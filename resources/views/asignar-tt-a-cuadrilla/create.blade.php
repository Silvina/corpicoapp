<!DOCTYPE html>
@extends('adminlte::page')

@section('title', 'Asignacion TT a Cuadrilla')

@section('content_header')
    <h1>Asignacion TT a Cuadrilla</h1>
@stop


@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                  
                    <div class="card-body">
                        <form method="POST" action="{{ route('asignar-tt-a-cuadrilla.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('cuadrilla.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
