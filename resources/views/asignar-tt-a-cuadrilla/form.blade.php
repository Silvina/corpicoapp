<!-- using https://github.com/istvan-ujjmeszaros/bootstrap-duallistbox -->
<!-- common libraries -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- plugin -->
<script src="https://www.virtuosoft.eu/code/bootstrap-duallistbox/bootstrap-duallistbox/v3.0.2/jquery.bootstrap-duallistbox.js"></script>

<link rel="stylesheet" type="text/css" href="https://www.virtuosoft.eu/code/bootstrap-duallistbox/bootstrap-duallistbox/v3.0.2/bootstrap-duallistbox.css">
<style>
	.moveall,
	.removeall {
		border: 1px solid #ccc !important;
	}

	.moveall::after {
		content: attr(title);
	}

	.removeall::after {
		content: attr(title);
	}

	/*  Custom styling form */
	.form-control option {
		padding: 10px;
		border-bottom: 1px solid #efefef;
	}
</style>

<div class="container">
	<div class="row col-md-12">
		<div class="box-body col-md-6">
			<div class="form-group col-md-12">
				{{ Form::label('Cuadrilla') }}
				<select name="cuadrilla" id="cuadrilla" class="form-control js-example-basic-single cuadrilla">
					<option value="#">Seleccione Cuadrilla</option>
					@foreach($cuadrillas as $cuadrilla)
					<option value="{{$cuadrilla->TC_ID}}">{{$cuadrilla->TC_DESCRIPCION}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<form id="asignacionTT">
		<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		<select id="selectTT" multiple="multiple" size=10 name="selectTT[]" title="selectTT[]">
			@foreach($tts as $ttgeneral)
			<option value="{{$ttgeneral['TIT_ID']}}" @if (array_key_exists($ttgeneral['TIT_ID'], $tt_asignadas)!==false) selected="selected" @endif>
				{{$ttgeneral['TIT_DESCRIPCION']}} - {{$ttgeneral['TIT_ABREVIATURA']}}
			</option>
			@endforeach
		</select>
		<br>
	</form>
	<div class="row">
		<div class="col-md-6 offset-md-6">
			<!-- AsignarTTaCuadrilla.update-->
			<button type="submit" class="btn btn-primary w-100">Guardar</button>
		</div>
	</div>
</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.js-example-basic-single').select2();

		$("#cuadrilla").change(function() {

			let tc_id = null;

			if ($("#cuadrilla option:selected").val())
				tc_id = $("#cuadrilla option:selected").val();

			if (tc_id) {
				$.ajax({
					data: {
						TC_ID: tc_id
					},
					dataType: 'json',
					type: 'POST',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: '/asignar-tt-a-cuadrilla_ajax'
				}).done(function(data) { //metodo que se ejecuta cuando ajax ha completado su ejecucion             

					$('#selectTT').children().remove().end();
					$.each(data.ttasignados, function(i, item) {
						//console.log(item);
						console.log(item.TC_ID);
						console.log(item.TIT_DESCRIPCION + ' - ' + item.TIT_ABREVIATURA);
						$('#selectTT').append('<option selected value="' + item.TC_ID + '">' + item.TIT_DESCRIPCION + ' - ' + item.TIT_ABREVIATURA + '</option>');
					});

					$.each(data.ttnoasignados, function(i, item) {
						//console.log(item);
						console.log(item.TC_ID);
						console.log(item.TIT_DESCRIPCION + ' - ' + item.TIT_ABREVIATURA);
						$('#selectTT').append('<option value="' + item.TC_ID + '">' + item.TIT_DESCRIPCION + ' - ' + item.TIT_ABREVIATURA + '</option>');
					});

					cuadrillasSelect.bootstrapDualListbox('refresh');
				});
			} else {
				$('#selectTT').children().remove().end();
			}
		});

	});
</script>
<script>
	var cuadrillasSelect = $('select[name="selectTT[]"]').bootstrapDualListbox({
		nonSelectedListLabel: 'TT sin asignar',
		selectedListLabel: 'TT asignados a cuadrilla',
		preserveSelectionOnMove: 'moved',
		moveAllLabel: 'Mover todo',
		removeAllLabel: 'Borrar todo'
	});
</script>