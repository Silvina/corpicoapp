@extends('adminlte::page')

@section('title', 'Tipos de Trabajo')

@section('content_header')
<h1>Tipo</h1>
@stop

@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="float-left">
						<span class="card-title">Ver Tipo de Trabajo</span>
					</div>
					<div class="float-right">
						<a class="btn btn-primary" href="{{ route('tipos-trabajo.index') }}"> Volver</a>
					</div>
				</div>

				<div class="card-body">

					<div class="form-group">
						<strong>Descripcion:</strong>
						{{ $tiposTrabajo->descripcion }}
					</div>
					<div class="form-group">
						<strong>Abreviatura:</strong>
						{{ $tiposTrabajo->abreviatura }}
					</div>
					<div class="form-group">
						<strong>Servicio Id:</strong>
						{{ $tiposTrabajo->servicio_id }}
					</div>
					<div class="form-group">
						<strong>Clasificacion Trabajo:</strong>
						{{ $tiposTrabajo->clasificacion_trabajo }}
					</div>
					<div class="form-group">
						<strong>Permite Eliminar:</strong>
						{{ $tiposTrabajo->permite_eliminar }}
					</div>
					<div class="form-group">
						<strong>Activo:</strong>
						{{ $tiposTrabajo->activo }}
					</div>
					<div class="form-group">
						<strong>Usuario Id:</strong>
						{{ $tiposTrabajo->usuario_id }}
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
@endsection