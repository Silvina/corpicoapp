@extends('adminlte::page')

@section('title', 'Tipo de Trabajo')

@section('content_header')
<h1>Tipo de trabajo</h1>
@stop

@section('content')
<div class="container-fluid">
	@if (session('success'))
	<div class="alert alert-success" role="success">
		{{ session('success') }}
	</div>
	@endif
	@if (session('error'))
	<div class="alert alert-error" role="error">
		{{ session('error') }}
	</div>
	@endif

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">

						<span id="card_title">
							{{ __('Tipos de Trabajo') }}
						</span>

						<div class="float-right">
							<a href="{{ route('tipos-trabajo.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Nuevo Tipo') }}
							</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="trabajos" class="table table-striped table-hover">
							<thead class="thead">
								<tr>
									<th>Descripcion</th>
									<th>Abreviatura</th>
									<th>Permite Eliminar</th>
									<th>Activo</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($tiposTrabajos as $tiposTrabajo)
								<tr>
									<td>{{ $tiposTrabajo->TIT_DESCRIPCION }}</td>
									<td>{{ $tiposTrabajo->TIT_ABREVIATURA }}</td>
									@if($tiposTrabajo->TIT_PERMITE_ELIMINAR==1)
									<td>Si</td>
									@else
									<td>No</td>
									@endif
									@if($tiposTrabajo->TIT_ACTIVO==1)
									<td>Si</td>
									@else
									<td>No</td>
									@endif
									<td>
										<form action="{{ route('tipos-trabajo.destroy',$tiposTrabajo->TIT_ID) }}" method="POST">
											<!-- <a class="btn btn-sm btn-primary " href="{{ route('tipos-trabajo.show',$tiposTrabajo->TIT_ID) }}"><i class="fa fa-fw fa-eye"></i></a> -->
											<a class="btn btn-sm btn-success" href="{{ route('tipos-trabajo.edit',$tiposTrabajo->TIT_ID) }}"><i class="fa fa-fw fa-edit"></i></a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
	$(document).ready(function() {
		$('#trabajos').DataTable({
			"language": {
				"search": "Buscar",
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"paginate": {
					"previous": "Anterior",
					"next": "Siguiente",
					"first": "Primero",
					"last": "Ultimo"

				}
			}
		});
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

<script type="text/javascript">
	$('.borrar').click(function(event) {
		var form = $(this).closest("form");
		var name = $(this).data("name");
		event.preventDefault();
		swal({
				title: `Eliminar Usuario`,
				text: "El Tipo de trabajo puede estar asignado a una cuadrilla. DESEA CONTINUAR?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})

			.then((willDelete) => {
				if (willDelete) {
					form.submit();
				}
			});
	});
</script>

@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection