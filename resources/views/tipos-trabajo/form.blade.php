<div class="box box-info padding-1">
	<div class="box-body row col-md-6">
		<div class="form-group col-md-6">
			{{ Form::label('descripcion') }}
			{{ Form::text('descripcion', $tiposTrabajo->descripcion, ['class' => 'form-control' . ($errors->has('descripcion') ? ' is-invalid' : ''), 'placeholder' => 'Descripcion']) }}
			{!! $errors->first('descripcion', '<div class="invalid-feedback">:message</div>') !!}
		</div>
		<div class="form-group col-md-6">
			{{ Form::label('abreviatura') }}
			{{ Form::text('abreviatura', $tiposTrabajo->abreviatura, ['class' => 'form-control' . ($errors->has('abreviatura') ? ' is-invalid' : ''), 'placeholder' => 'Abreviatura']) }}
			{!! $errors->first('abreviatura', '<div class="invalid-feedback">:message</div>') !!}
		</div>

		<div class="form-group col-md-6">
			{{ Form::label('permite_eliminar') }}
			{{ Form::checkbox('permite_eliminar', $tiposTrabajo->permite_eliminar, ['class' => 'form-control' . ($errors->has('permite_eliminar') ? ' is-invalid' : ''), 'placeholder' => 'Permite Eliminar']) }}
			{!! $errors->first('permite_eliminar', '<div class="invalid-feedback">:message</div>') !!}
		</div>
		<div class="form-group col-md-6">
			{{ Form::label('activo') }}
			{{ Form::checkbox('activo', $tiposTrabajo->activo, ['class' => 'form-control' . ($errors->has('activo') ? ' is-invalid' : ''), 'placeholder' => 'Activo']) }}
			{!! $errors->first('activo', '<div class="invalid-feedback">:message</div>') !!}
		</div>
		<!--        <div class="form-group col-md-12">
            {{ Form::label('servicio_id') }}
            {{ Form::text('servicio_id', $tiposTrabajo->servicio_id, ['class' => 'form-control' . ($errors->has('servicio_id') ? ' is-invalid' : ''), 'placeholder' => 'Servicio Id']) }}
            {!! $errors->first('servicio_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>-->
	</div>
	<div class="box-body col-md-6">

		<div class="form-group col-md-6" style=" display:none ">
			{{ Form::label('usuario_id') }}
			{{ Form::text('usuario_id', $tiposTrabajo->usuario_id, ['class' => 'form-control' . ($errors->has('usuario_id') ? ' is-invalid' : ''), 'placeholder' => 'Usuario Id']) }}
			{!! $errors->first('usuario_id', '<div class="invalid-feedback">:message</div>') !!}
		</div>

	</div>
</div>
<div class="box-footer mt20">
	<button type="submit" class="btn btn-primary">Guardar</button>
</div>
</div>