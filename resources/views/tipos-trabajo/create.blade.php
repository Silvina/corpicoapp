@extends('adminlte::page')

@section('title', 'Tipo de Trabajo')

@section('content_header')
<h1>Tipo de Trabajo</h1>
@stop


@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-default">
				<div class="card-header">
					<span class="card-title">Nuevo Tipo de Trabajo</span>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('tipos-trabajo.store') }}" role="form" enctype="multipart/form-data">
						@csrf

						@include('tipos-trabajo.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection