@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
<h1>Usuarios</h1>
@stop


@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="float-left">
						<span class="card-title">Mostrar Usuario</span>
					</div>
					<div class="float-right">
						<a class="btn btn-primary" href="{{ route('usuarios.index') }}"> Volver</a>
					</div>
				</div>

				<div class="card-body row col-md-12">

					<div class="form-group col-md-4">
						<strong>Apellido:</strong>
						{{ $usuario->apellido }}
					</div>
					<div class="form-group col-md-4">
						<strong>Nombre:</strong>
						{{ $usuario->nombre }}
					</div>

					<div class="form-group col-md-4">
						<strong>Cuadrilla:</strong>
						{{ $cuadrilla->descripcion }}
					</div>
					<div class="form-group  col-md-4">
						<strong>Nro Operario:</strong>
						{{ $usuario->operario }}
					</div>
					<div class="form-group col-md-4">
						<strong>Nombre Usuario:</strong>
						{{ $usuario->nombre_usuario }}
					</div>
					<div class="form-group col-md-4">
						<strong>Servicio:</strong>
						{{ $servicio->descripcion }}
					</div>
					<div class="form-group col-md-4">
						<strong>Sector:</strong>
						{{ $sector->descripcion }}
					</div>
					<div class="form-group col-md-4">
						<strong>Rol:</strong>
						{{ $rol->descripcion }}
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
@endsection