@extends('adminlte::page')

@section('title', 'Usuario')
@section('plugins.Select2',true)
@section('content_header')
<h1>Usuario</h1>
@stop


@section('content')
<section class="content container-fluid">


	<div class="row">
		<div class="col-md-12">

			@includeif('partials.errors')
			<div class="card card-default">
				<div class="card-header">
					<span class="card-title">Nuevo Usuario</span>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('usuarios.store') }}" role="form" enctype="multipart/form-data">
						@csrf
						@include('usuario.form')
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection