		@extends('adminlte::page')
		@section('title', 'Usuario')
		@section('content_header')
		<h1>Modificar Usuario</h1>
		@stop

		@section('plugins.Select2', true)

		@section('content')
		<section class="content container-fluid">
			<div class="">
				<div class="col-md-12">

					@includeif('partials.errors')

					<div class="card card-default">
						<div class="card-header">
							<span class="card-title">Modificar Operario </span><span style="float:right; border: 2px solid green"> Solo podrá <b>eliminar el usuario</b> desde el listado USUARIOS</span>
						</div>
						<div class="card-body">
							<form method="POST" action="{{ route('usuarios.update', $id) }}" role="form" enctype="multipart/form-data">

								{{ method_field('PATCH') }}
								@csrf

								<div class="box box-info padding-1">
									<div class="box-body row col-md-12">
										<div class="col-md-6">

		
											<div class="form-group col-md-12">
												{{ Form::label('Servicio ') }}<br>
												<input type="text" name="servicio" class="form-control" disabled value="{{$nombreServicio}}">
												<div class="form-group col-md-12">
															{{ Form::hidden('servicio', $id_servicio, ['class' => 'form-control' ]) }}
														</div>
											</div>

											<div class="box-body row col-md-12" style="border:3px double; border-radius:10px">
												<div class="form-group col-md-6">
													<span>Rol del usuario:</span>
												</div>
												<div class="form-group checkbox col-md-6">
													@if($usuarioOpe != [])
													<input name="operario" checked value="on" type="checkbox" id="operario" class="operario" />
													@else
													<input name="operario" type="checkbox" value="off" id="operario" class="operario" />
													@endif
													<label for="operario">Operario </label><br>
													@if($usuarioSup != [])
													<input name="supervisor" value="on" checked type="checkbox" id="supervisor" class="supervisor" />
													@else
													<input name="supervisor" value="off" type="checkbox" id="supervisor" class="supervisor" />
													@endif

													<label for="supervisor">Supervisor </label><br>
												</div>
											</div>
										</div>
										<div class="box-body row col-md-6">
											<div class="form-group col-md-12">
												{{ Form::label('Operario') }}<br>
												{{$usuarioGea->OPE_DESCRIPCION}} - {{$usuarioGea->OPE_ID}}
											</div>
											<div class="form-group col-md-6">
												{{ Form::label('Apellido') }}
												{{ Form::text('apellido', $usuario->Apellido, ['class' => 'form-control', 'placeholder' => 'Apellido', 'required'=>'required']) }}
											</div>
											<div class="form-group col-md-6">
												{{ Form::label('Nombre') }}
												{{ Form::text('nombre', $usuario->Nombre, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required'=>'required']) }}
											</div>

										</div>

										<div class="form-group col-md-6" id="contenidoOperario">
											<hr>
											<div class="card card-default">

												<div class="card-header" style="background-color:#007e3a">
													<span class="card-title">Registro como Operario</span>
												</div>
												<div class="card-body">
													<div class="box box-info padding-1">
														<div class="box-body row col-md-12">
															<div class="form-group col-md-12">
																{{ Form::label('Nombre Usuario Sugerido') }}
																{{ Form::text('usuarioOperario', $nombreUsuarioOpe, ['class' => 'form-control', 'id'=>'usuarioOperario', 'placeholder' => 'Nombre Usuario Operario']) }}
															</div>
														</div>
														<div class="box-body row col-md-12">

															{{ Form::label('Sector') }}: {{$sectorOpeNombre}}

															<select name='sector_id_operario' style="width:100%" id='sector_id_operario' class=" row form-control js-example-basic-single">
																<option value="#">Seleccione Nuevo Sector</option>
																@foreach($sectores as $sector)
																<option value="{{$sector['SEC_ID']}}">{{$sector['SEC_DESCRIPCION']}}</option>
																@endforeach
															</select>
														</div>
														<hr>
														<input name="chec1" checked type="checkbox" id="chec1" onchange="comprobar1();" />
														<label for="chec1">Generar/Modificar Contraseña automáticamente </label><br>
														<span style="font-size:12px">Ej: Us: <b>mdelvalle</b> - Formato de clave: <b>me1948dl</b></span>

												
														<div class="form-group col-md-12">
															{{ Form::label('Nueva Contraseña') }}
															{{ Form::password('newPasswordOperario', ['class' => 'form-control','id'=>'newPasswordOperario', 'attribute' => 'value','readonly']) }}
														</div>
														<div class="form-group col-md-12">
															{{ Form::label('Confirmar Contraseña') }}
															{{ Form::password('confirmNewPasswordOperario', ['class' => 'form-control','id'=>'confirmNewPasswordOperario', 'attribute' => 'value','readonly']) }}
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group col-md-6" id="contenidoSupervisor">
											<hr>
											<div class="card card-default">

												<div class="card-header" style="background-color:#007e3a">
													<span class="card-title">Registro como Supervisor</span>
												</div>
												<div class="card-body">
													<div class="box box-info padding-1">
														<div class="box-body row col-md-12">
															<div class="form-group col-md-12">
																{{ Form::label('Nombre Usuario Sugerido') }}
																{{ Form::text('usuarioSupervisor', $nombreUsuarioSup, ['class' => 'form-control', 'id'=>'usuarioSupervisor', 'placeholder' => 'Nombre Usuario Supervisor', 'required']) }}

															</div>
														</div>
														<div class="box-body row col-md-12">

															{{ Form::label('Sector') }}: {{$sectorSupNombre}}
															<select name='sector_id_supervisor' style="width:100%" id='sector_id_supervisor' class=" row form-control js-example-basic-single">
																<option value="#">Seleccione Nuevo Sector</option>

																@foreach($sectores as $sector)
																<option value="{{$sector['SEC_ID']}}">{{$sector['SEC_DESCRIPCION']}}</option>
																@endforeach
															</select>
														</div>
														<hr>
														<input name="chec2" checked type="checkbox" id="chec2" onchange="comprobar2();" />
														<label for="chec2">Generar/Modificar Contraseña automática </label><br>
														<span style="font-size:12px">Ej: Us: <b>smdelvalle</b> - Formato de clave: <b>se1948ml</b></span>
														<div class="form-group col-md-12">
															{{ Form::label('Nueva Contraseña') }}
															{{ Form::password('newPasswordSupervisor', ['class' => 'form-control','id'=>'newPasswordSupervisor', 'attribute' => 'value','readonly']) }}
														</div>
														<div class="form-group col-md-12">
															{{ Form::label('Confirmar Contraseña') }}
															{{ Form::password('confirmNewPasswordSupervisor', ['class' => 'form-control','id'=>'confirmNewPasswordSupervisor', 'attribute' => 'value','readonly']) }}
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="box-footer mt20 col-md-12">
											<hr>
											<button type="submit" class="btn btn-primary">Guardar</button>
										</div>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		@endsection


		@section('js')
		<script>
			$(document).ready(function() {
				$('.js-example-basic-single').select2();
			});

			function comprobar1() {
				document.getElementById('newPasswordOperario').readOnly = document.getElementById("chec1").checked;
				document.getElementById('confirmNewPasswordOperario').readOnly = document.getElementById("chec1").checked;
			}

			function comprobar2() {
				document.getElementById('newPasswordSupervisor').readOnly = document.getElementById("chec2").checked;
				document.getElementById('confirmNewPasswordSupervisor').readOnly = document.getElementById("chec2").checked;
			}
		</script>
		<script type="text/javascript">
			if (document.getElementById("operario").value == 'on') {
				$('#contenidoOperario').show();
			} else {
				$('#contenidoOperario').hide();
			}

			$(function() {

				$('.operario').change(function() {
					if (!$(this).prop('checked')) {
						$('#contenidoOperario').hide();
					} else {
						$('#contenidoOperario').show();
					}

				})
			});


			if (document.getElementById("supervisor").value == 'on') {
				$('#contenidoSupervisor').show();
			} else {
				$('#contenidoSupervisor').hide();
			}

			$(function() {
				$('.supervisor').change(function() {
					if (!$(this).prop('checked')) {
						$('#contenidoSupervisor').hide();
					} else {
						$('#contenidoSupervisor').show();
					}

				})
			});

			$("#usuarioOperario").change(function() {
				Swal.fire({
					html: `<h1>Sugerencia</h1>
			<p>El campo <strong>Usuario Sugerido del Operario</strong> ha cambiado. Debería actualizar la contraseña</p>
			`,
				});
			});

			$("#usuarioSupervisor").change(function() {
				Swal.fire({
					html: `<h1>Sugerencia</h1>
			<p>El campo <strong>Usuario Sugerido del Supervisor</strong> ha cambiado. Debería actualizar la contraseña</p>
			`,
				});
			});
		</script>
		@endsection