<div class="box box-info padding-1">
	<div class="box-body row col-md-12">

		<div class="box-body row col-md-6">

			@if($rol==2)
			<div class="form-group col-md-12">
				{{ Form::label('Servicio ') }}<br>
				<input type="text" class="form-control" disabled value="{{$nombreServicio}}">
			</div>
			<!-- <div class="form-group col-md-12">
					{{ Form::label('Sector: ') }}<br>
					<input type="text" class="form-control" disabled value="	{{$nombreSector}}">
				
			</div>	 -->
			@else
			<div class="form-group col-md-12">
				{{ Form::label('Servicio ') }}<br>
				<input type="text" class="form-control" disabled value="	{{$nombreServicio}}">

			</div>


			@endif

			<div class=" row form-group col-md-12" style="border:3px double; border-radius:10px; height:80px">
				<div class="form-group col-md-6">
					<span>Rol del usuario:</span>
				</div>
				<div class="form-group checkbox col-md-6">
					<input name="operario" checked type="checkbox" id="operario" class="operario" />
					<label for="operario">Operario </label><br>
					<input name="supervisor" checked value='on' type="checkbox" id="supervisor" class="supervisor" />
					<label for="supervisor">Supervisor </label><br>
				</div>
			</div>

		</div>

		<div class="box-body row col-md-6">
			<div class=" row form-group col-md-12" style="margin-left:10px; border:3px double; border-radius:10px">
				<div class="form-group col-md-9">
					{{ Form::label('Operario') }}
					<select name="operario_id" id="operario_id" class="form-control js-example-basic-single">
						<option value="#">Seleccione Operario</option>
						@foreach($usuariosGea as $usuarioGea)
						<option value="{{$usuarioGea->OPE_ID}}">{{$usuarioGea->OPE_DESCRIPCION}} - {{$usuarioGea->OPE_ID}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('Nuevo Op.') }}
					<abbr title="Agregar nuevo operario si no existe en el listado">
						<button style="background-color:green" type="button" class="btn btn-success " data-toggle="modal" data-target="#modalCrearUsuarioGeasys">+</button>
					</abbr>
				</div>
			</div>

			<div class="form-group col-md-6">
				{{ Form::label('Apellido') }}
				{{ Form::text('apellido', null, ['class' => 'form-control', 'placeholder' => 'Apellido']) }}
			</div>

			<div class="form-group col-md-6">
				{{ Form::label('Nombre') }}
				{{ Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
			</div>


		</div>
		<div class="form-group col-md-6" id="contenidoOperario">
			<hr>
			<div class="card card-default">

				<div class="card-header" style="background-color:#007e3a">
					<span class="card-title">Registro como Operario</span>
				</div>
				<div class="card-body col-md-12">
					<div class="box box-info padding-1">
						<div class="box-body row col-md-12">
							<div class="form-group col-md-12">
								{{ Form::label('Usuario Sugerido') }}
								{{ Form::text('usuarioOperario', '', ['class' => 'form-control','id' => 'usuarioOperario', 'placeholder' => 'Sugerencia: Lucas Montero --> Usuario: lmontero']) }}
							</div>

						</div>
						<div class="box-body row col-md-12">

							{{ Form::label('Sector') }}

							<select name='sector_id_operario' style="width:100%" id='sector_id_operario' class=" row form-control js-example-basic-single">
								<option value="#">Seleccione Sector</option>
								@foreach($sectores as $sector)
								<option value="{{$sector['SEC_ID']}}">{{$sector['SEC_DESCRIPCION']}}</option>
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group col-md-12">
							<input name="chec1" checked type="checkbox" id="chec1" onchange="comprobar1();" />
							<label for="chec1">Generar/Modificar Contraseña automáticamente </label><br>
							<span style="font-size:12px">Ej: Us: <b>mdelvalle</b> - Formato de clave: <b>me1948dl</b></span>
						</div>
						<div class="form-group col-md-12">
							{{ Form::hidden('', '', ['class' => 'form-control' ]) }}
						</div>
						<div class="form-group col-md-12">
							{{ Form::label('Nueva Contraseña') }}
							{{ Form::password('newPasswordOperario', ['class' => 'form-control','id'=>'newPasswordOperario', 'attribute' => 'value','readonly']) }}
						</div>
						<div class="form-group col-md-12">
							{{ Form::label('Confirmar Contraseña') }}
							{{ Form::password('confirmNewPasswordOperario', ['class' => 'form-control','id'=>'confirmNewPasswordOperario', 'attribute' => 'value','readonly']) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group col-md-6" id="contenidoSupervisor">
			<hr>
			<div class="card card-default">

				<div class="card-header" style="background-color:#007e3a">
					<span class="card-title">Registro como Supervisor</span>
				</div>
				<div class="card-body">
					<div class="box box-info padding-1">
						<div class="box-body row col-md-12">
							<div class="form-group col-md-12">
								{{ Form::label('Usuario Sugerido') }}
								{{ Form::text('usuarioSupervisor','', ['class' => 'form-control','id'=>'usuarioSupervisor', 'placeholder' => 'Sugerencia: Lucas Montero --> Usuario: slmontero']) }}
							</div>
						</div>
						<div class="box-body row col-md-12">

							{{ Form::label('Sector') }}

							<select name='sector_id_supervisor' style="width:100%" id='sector_id_supervisor' class=" row form-control js-example-basic-single">
								<option value="#">Seleccione Sector</option>
								@foreach($sectores as $sector)
								<option value="{{$sector['SEC_ID']}}">{{$sector['SEC_DESCRIPCION']}}</option>
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group col-md-12">
							<input name="chec2" checked type="checkbox" id="chec2" onchange="comprobar2();" />
							<label for="chec2">Generar/Modificar Contraseña automática </label><br>
							<small>Ej: Us: <b>smdelvalle</b> - Formato de clave: <b>se1948ml</b></small>
						</div>
						<div class="form-group col-md-12">
							{{ Form::label('Nueva Contraseña') }}
							{{ Form::password('newPasswordSupervisor', ['class' => 'form-control','id'=>'newPasswordSupervisor', 'attribute' => 'value','readonly']) }}
						</div>
						<div class="form-group col-md-12">
							{{ Form::label('Confirmar Contraseña') }}
							{{ Form::password('confirmNewPasswordSupervisor', ['class' => 'form-control','id'=>'confirmNewPasswordSupervisor', 'attribute' => 'value', 'readonly']) }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row box-body mt20 col-md-12">
		<br>
		<button type="submit" class="btn btn-primary">Guardar</button>
	</div>
</div>

<!-- Modal -->
<div id="modalCrearUsuarioGeasys" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header " style="background-color:#007e3a">
				<h4 class="modal-title">Nuevo Usuario Geasys</h4>

				<button type="button" class="close left" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group col-md-12">
					{{ Form::label('Apellido y Nombre') }}
					{{ Form::text('descripcionGeasys', null, ['class' => 'form-control', 'placeholder' => 'Ingrese apellido y nombre']) }}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
		</div>

	</div>
</div>

</div>

@section('js')
<script>
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
	});

	function comprobar1() {
		document.getElementById('newPasswordOperario').readOnly = document.getElementById("chec1").checked;
		document.getElementById('confirmNewPasswordOperario').readOnly = document.getElementById("chec1").checked;
	}

	function comprobar2() {
		document.getElementById('newPasswordSupervisor').readOnly = document.getElementById("chec2").checked;
		document.getElementById('confirmNewPasswordSupervisor').readOnly = document.getElementById("chec2").checked;
	}
</script>
<script type="text/javascript">
	$(function() {
		$('.operario').change(function() {
			if (!$(this).prop('checked')) {
				$('#contenidoOperario').hide();
			} else {
				$('#contenidoOperario').show();
			}

		})
	});

	$(function() {
		$('.supervisor').change(function() {
			if (!$(this).prop('checked')) {
				$('#contenidoSupervisor').hide();
			} else {
				$('#contenidoSupervisor').show();
			}

		})
	});
</script>
@endsection