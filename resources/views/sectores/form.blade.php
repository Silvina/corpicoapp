<div class="box box-info padding-1">
	<div class="box-body row">

		<div class="form-group col-md-4">
			{{ Form::label('Servicio:') }}<br>
			{{$nombreServicio}}
		</div>

		<div class="form-group col-md-4">
			{{ Form::label('nombre del sector') }}
			{{ Form::text('descripcion', $nombreSector == '' ? null : $nombreSector, ['class' => 'form-control' . ($errors->has('descripcion') ? ' is-invalid' : ''), 'placeholder' => 'Descripcion']) }}
		</div>

	</div>
	<div class="box-footer mt20">
		<button type="submit" class="btn btn-primary">Guardar</button>
	</div>
</div>