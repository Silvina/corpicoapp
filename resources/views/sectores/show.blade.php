@extends('adminlte::page')

@section('title', 'Sector')

@section('content_header')
<h1>Ver Sector</h1>
@stop

@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="float-left">
						<span class="card-title">Mostrar Sector</span>
					</div>
					<div class="float-right">
						<a class="btn btn-primary" href="{{ route('sectores.index') }}"> Volver</a>
					</div>
				</div>

				<div class="card-body">

					<div class="form-group">
						<strong>Servicio Id:</strong>
						{{ $sector->servicio_id }}
					</div>
					<div class="form-group">
						<strong>Descripcion:</strong>
						{{ $sector->descripcion }}
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
@endsection