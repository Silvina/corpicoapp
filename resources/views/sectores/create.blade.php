@extends('adminlte::page')

@section('title', 'Sector')

@section('content_header')
<h1>Sector</h1>
@stop


@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-default">
				<div class="card-header">
					<span class="card-title">Nuevo Sector</span>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('sectores.store') }}" role="form" enctype="multipart/form-data">
						@csrf

						@include('sectores.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection