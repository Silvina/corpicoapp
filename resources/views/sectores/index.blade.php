@extends('adminlte::page')

@section('title', 'Sectores')

@section('content_header')
<h1>Sectores</h1>
@stop

@section('content')
<div class="container-fluid">
	@if (session('success'))
	<div class="alert alert-success" role="success">
		{{ session('success') }}
	</div>
	@endif
	@if (session('error'))
	<div class="alert alert-error" role="error">
		{{ session('error') }}
	</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">

						<span id="card_title">
							{{ __('Lista de Sectores') }}s
						</span>

						<div class="float-right">
							<a href="{{ route('sectores.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Nuevo Sector') }}
							</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="sectores" class="table table-striped table-hover">
							<thead class="thead">
								<tr>
									<th>Descripcion</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($arraySectores as $sector)
								<tr>
									<td>{{ $sector['SEC_DESCRIPCION'] }}</td>
									<td>
										<form action="{{ route('sectores.destroy',$sector['SEC_ID']) }}" method="POST">
											<a class="btn btn-sm btn-success" href="{{ route('sectores.edit',$sector['SEC_ID']) }}"><i class="fa fa-fw fa-edit"></i> </a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm borrar"><i class="fa fa-fw fa-trash"></i> </button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
	$(document).ready(function() {
		$('#sectores').DataTable({
			"language": {
				"search": "Buscar",
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"paginate": {
					"previous": "Anterior",
					"next": "Siguiente",
					"first": "Primero",
					"last": "Ultimo"

				}
			}
		});
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

<script type="text/javascript">
		$('.borrar').click(function(event) {
			var form = $(this).closest("form");
			var name = $(this).data("name");
			event.preventDefault();
			swal({
					title: `Eliminar Sector`,
					text: "Este sector puede tener motivos asignados. DESEA CONTINUAR?",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})

				.then((willDelete) => {
					if (willDelete) {
						form.submit();
					}
				});
		});
	</script>
@endsection