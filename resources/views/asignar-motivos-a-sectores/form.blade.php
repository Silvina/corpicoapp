<!-- using https://github.com/istvan-ujjmeszaros/bootstrap-duallistbox -->

<!-- common libraries -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<!-- plugin -->
<script src="https://www.virtuosoft.eu/code/bootstrap-duallistbox/bootstrap-duallistbox/v3.0.2/jquery.bootstrap-duallistbox.js"></script>

<link rel="stylesheet" type="text/css" href="https://www.virtuosoft.eu/code/bootstrap-duallistbox/bootstrap-duallistbox/v3.0.2/bootstrap-duallistbox.css">
<style>
	/* .moveall,
	.removeall {
		border: 1px solid #ccc !important;

		&:hover {
			background: #efefef;
		}
	} */

	.moveall::after {
		content: attr(title);

	}

	.removeall::after {
		content: attr(title);
	}

	/* // Custom styling form */
	.form-control option {
		padding: 10px;
		border-bottom: 1px solid #efefef;
	}
</style>

<div class="container">
	<div class="row col-md-12">
		<div class="box-body col-md-3"></div>
		<div class="box-body col-md-6">
			<div class="form-group col-md-12">
				{{ Form::label('Cuadrilla') }}
				<select name="id_cuadrilla" id="id_cuadrilla" class="form-control js-example-basic-single">
					<option value="#">Seleccione Cuadrilla</option>
					@foreach($list_cuadrillas as $cuadrilla)
					<option value="{{$cuadrilla['TC_ID']}}">{{$cuadrilla['TC_DESCRIPCION']}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="box-body col-md-3"></div>
	</div>
	<form id="asignacionTT">
		<select multiple="multiple" size=10 name="selectTT[]" title="selectTT[]">
			@foreach($tts as $ttgeneral)
			<option value="{{$ttgeneral['TIT_ID']}}">{{$ttgeneral['TIT_DESCRIPCION']}} - {{$ttgeneral['TIT_ABREVIATURA']}}</option>
			@endforeach
		</select>
		<br>
	</form>
	<div class="row">
		<div class="col-md-6 offset-md-6">
			<!-- AsignarTTaCuadrilla.update-->
			<button type="submit" class="btn btn-primary w-100">Guardar</button>
		</div>
	</div>
</div>
</div>
</div>

<script>
	var demo1 = $('select[name="selectTT[]"]').bootstrapDualListbox({
		nonSelectedListLabel: 'TT sin asignar',
		selectedListLabel: 'TT asignados a cuadrilla',
		preserveSelectionOnMove: 'moved',
		moveAllLabel: 'Mover todo',
		removeAllLabel: 'Borrar todo'
	});

	$("#asignacionTT").submit(function() {
		alert($('[name="selectTT[]"]').val());
		return false;
	});
</script>
<!-- Agregamos la libreria Jquery -->
<!--<script type="text/javascript" src="jquery-3.2.0.min.js"></script>-->
<script type="text/javascript">
	$(document).ready(function() {


		// var asignados = $('#selectTT[]');
		// var asignados_sel = $('#selectTT[]_sel');

		//Ejecutar accion al cambiar de opcion en el select de las bandas
		$('#id_cuadrilla').change(function() {
			var id_cuadrilla = $(this).val(); //obtener el id seleccionado
			var url = "/asignar-tt-a-cuadrilla/obtenerCuadrillas/" + id_cuadrilla;
			console.log(url);
			if (id_cuadrilla !== '') { //verificar haber seleccionado una opcion valida

				$.ajax({
					data: {
						id_cuadrilla: id_cuadrilla
					}, //variables o parametros a enviar, formato => nombre_de_variable:contenido
					dataType: 'html', //tipo de datos que esperamos de regreso
					type: 'POST', //mandar variables como post o get
					url: url //url que recibe las variables
				}).done(function(data) { //metodo que se ejecuta cuando ajax ha completado su ejecucion             

					asignados.html(data); //establecemos el contenido html de asignados con la informacion que regresa ajax             
					asignados.prop('disabled', false); //habilitar el select
				});


			} else { //en caso de seleccionar una opcion no valida
				asignados.val(''); //seleccionar la opcion "- Seleccione -", osea como reiniciar la opcion del select
				asignados.prop('disabled', true); //deshabilitar el select
			}
		});

		//mostrar una leyenda 
		$('#asignados').change(function() {
			$('#selectTT[]_sel').html($(this).val() + ' - ' + $('#asignados option:selected').text());
		});

	});
</script>