<!DOCTYPE html>
@extends('adminlte::page')

@section('title', 'Asignacion Motivos a Sector')

@section('content_header')
<h1>Asignacion Motivos a Sectores</h1>
@stop


@section('content')
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-12">

			@includeif('partials.errors')

			<div class="card card-default">

				<div class="card-body">
					<form method="POST" action="{{ route('asignar-motivos-a-sectores.store') }}" role="form" enctype="multipart/form-data">
						@csrf

						@include('sector.form')

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection