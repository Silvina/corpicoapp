@extends('adminlte::page')

@section('title', 'Servicio')

@section('content_header')
    <h1>Servicio</h1>
@stop


@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Motivos Trabajo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('motivos-trabajo.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Tipo Trabajo Id:</strong>
                            {{ $motivosTrabajo->tipo_trabajo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Descripcion:</strong>
                            {{ $motivosTrabajo->descripcion }}
                        </div>
                        <div class="form-group">
                            <strong>Abreviatura:</strong>
                            {{ $motivosTrabajo->abreviatura }}
                        </div>
                        <div class="form-group">
                            <strong>Dias Resolucion:</strong>
                            {{ $motivosTrabajo->dias_resolucion }}
                        </div>
                        <div class="form-group">
                            <strong>Alerta Estado Luego Generar:</strong>
                            {{ $motivosTrabajo->alerta_estado_luego_generar }}
                        </div>
                        <div class="form-group">
                            <strong>Estado Luego Generar:</strong>
                            {{ $motivosTrabajo->estado_luego_generar }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Colocacion:</strong>
                            {{ $motivosTrabajo->solicita_colocacion }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Retiro:</strong>
                            {{ $motivosTrabajo->solicita_retiro }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Informativo:</strong>
                            {{ $motivosTrabajo->solicita_informativo }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Precinto Hab:</strong>
                            {{ $motivosTrabajo->solicita_precinto_hab }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Precinto Med:</strong>
                            {{ $motivosTrabajo->solicita_precinto_med }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Instalacion:</strong>
                            {{ $motivosTrabajo->solicita_instalacion }}
                        </div>
                        <div class="form-group">
                            <strong>Solicita Materiales:</strong>
                            {{ $motivosTrabajo->solicita_materiales }}
                        </div>
                        <div class="form-group">
                            <strong>Permite Eliminar:</strong>
                            {{ $motivosTrabajo->permite_eliminar }}
                        </div>
                        <div class="form-group">
                            <strong>Procesa Tomaestado:</strong>
                            {{ $motivosTrabajo->procesa_tomaestado }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $motivosTrabajo->activo }}
                        </div>
                        <div class="form-group">
                            <strong>Usuario Id:</strong>
                            {{ $motivosTrabajo->usuario_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
