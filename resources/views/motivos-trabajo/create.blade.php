@extends('adminlte::page')

@section('title', 'Motivo de Trabajo')

@section('content_header')
    <h1>Motivo de Trabajo</h1>
@stop


@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Nuevo Motivo de Trabajo</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('motivos-trabajo.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('motivos-trabajo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
