<div class="box box-info padding-1">
    <div class="box-body row col-md-12">
        
        <div class="form-group col-md-4">
            {{ Form::label('tipo_trabajo_id') }}
            {{ Form::text('tipo_trabajo_id', $motivosTrabajo->tipo_trabajo_id, ['class' => 'form-control' . ($errors->has('tipo_trabajo_id') ? ' is-invalid' : ''), 'placeholder' => 'Tipo Trabajo Id']) }}
            {!! $errors->first('tipo_trabajo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-4">
            {{ Form::label('descripcion') }}
            {{ Form::text('descripcion', $motivosTrabajo->descripcion, ['class' => 'form-control' . ($errors->has('descripcion') ? ' is-invalid' : ''), 'placeholder' => 'Descripcion']) }}
            {!! $errors->first('descripcion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-4">
            {{ Form::label('abreviatura') }}
            {{ Form::text('abreviatura', $motivosTrabajo->abreviatura, ['class' => 'form-control' . ($errors->has('abreviatura') ? ' is-invalid' : ''), 'placeholder' => 'Abreviatura']) }}
            {!! $errors->first('abreviatura', '<div class="invalid-feedback">:message</div>') !!}
        </div>
				<div class="form-group col-md-4">
            {{ Form::label('usuario_id') }}
            {{ Form::text('usuario_id', $motivosTrabajo->usuario_id, ['class' => 'form-control' . ($errors->has('usuario_id') ? ' is-invalid' : ''), 'placeholder' => 'Usuario Id']) }}
            {!! $errors->first('usuario_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
				<div class="form-group col-md-4">
            {{ Form::label('dias_resolucion') }}
            {{ Form::text('dias_resolucion', $motivosTrabajo->dias_resolucion, ['class' => 'form-control' . ($errors->has('dias_resolucion') ? ' is-invalid' : ''), 'placeholder' => 'Dias Resolucion']) }}
            {!! $errors->first('dias_resolucion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
</div>
<div class="box-body  row col-md-12">

     
        <div class="form-group col-md-3">
            {{ Form::label('alerta_estado_luego_generar') }}
            {{ Form::checkbox('alerta_estado_luego_generar', $motivosTrabajo->alerta_estado_luego_generar, ['class' => 'form-control' . ($errors->has('alerta_estado_luego_generar') ? ' is-invalid' : ''), 'placeholder' => 'Alerta Estado Luego Generar']) }}
            {!! $errors->first('alerta_estado_luego_generar', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('estado_luego_generar') }}
            {{ Form::checkbox('estado_luego_generar', $motivosTrabajo->estado_luego_generar, ['class' => 'form-control' . ($errors->has('estado_luego_generar') ? ' is-invalid' : ''), 'placeholder' => 'Estado Luego Generar']) }}
            {!! $errors->first('estado_luego_generar', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('solicita_colocacion') }}
            {{ Form::checkbox('solicita_colocacion', $motivosTrabajo->solicita_colocacion, ['class' => 'form-control' . ($errors->has('solicita_colocacion') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Colocacion']) }}
            {!! $errors->first('solicita_colocacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
				<div class="form-group col-md-3">
            {{ Form::label('solicita_retiro') }}
            {{ Form::checkbox('solicita_retiro', $motivosTrabajo->solicita_retiro, ['class' => 'form-control' . ($errors->has('solicita_retiro') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Retiro']) }}
            {!! $errors->first('solicita_retiro', '<div class="invalid-feedback">:message</div>') !!}
        </div>
</div>
<div class="box-body  row col-md-12">

        <div class="form-group col-md-3">
            {{ Form::label('solicita_informativo') }}
            {{ Form::checkbox('solicita_informativo', $motivosTrabajo->solicita_informativo, ['class' => 'form-control' . ($errors->has('solicita_informativo') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Informativo']) }}
            {!! $errors->first('solicita_informativo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('solicita_precinto_hab') }}
            {{ Form::checkbox('solicita_precinto_hab', $motivosTrabajo->solicita_precinto_hab, ['class' => 'form-control' . ($errors->has('solicita_precinto_hab') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Precinto Hab']) }}
            {!! $errors->first('solicita_precinto_hab', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('solicita_precinto_med') }}
            {{ Form::checkbox('solicita_precinto_med', $motivosTrabajo->solicita_precinto_med, ['class' => 'form-control' . ($errors->has('solicita_precinto_med') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Precinto Med']) }}
            {!! $errors->first('solicita_precinto_med', '<div class="invalid-feedback">:message</div>') !!}
        </div>
				<div class="form-group col-md-3">
            {{ Form::label('solicita_instalacion') }}
            {{ Form::checkbox('solicita_instalacion', $motivosTrabajo->solicita_instalacion, ['class' => 'form-control' . ($errors->has('solicita_instalacion') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Instalacion']) }}
            {!! $errors->first('solicita_instalacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
</div>
<div class="box-body  row col-md-12">
        
        <div class="form-group col-md-3">
            {{ Form::label('solicita_materiales') }}
            {{ Form::checkbox('solicita_materiales', $motivosTrabajo->solicita_materiales, ['class' => 'form-control' . ($errors->has('solicita_materiales') ? ' is-invalid' : ''), 'placeholder' => 'Solicita Materiales']) }}
            {!! $errors->first('solicita_materiales', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('permite_eliminar') }}
            {{ Form::checkbox('permite_eliminar', $motivosTrabajo->permite_eliminar, ['class' => 'form-control' . ($errors->has('permite_eliminar') ? ' is-invalid' : ''), 'placeholder' => 'Permite Eliminar']) }}
            {!! $errors->first('permite_eliminar', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('procesa_tomaestado') }}
            {{ Form::checkbox('procesa_tomaestado', $motivosTrabajo->procesa_tomaestado, ['class' => 'form-control' . ($errors->has('procesa_tomaestado') ? ' is-invalid' : ''), 'placeholder' => 'Procesa Tomaestado']) }}
            {!! $errors->first('procesa_tomaestado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('activo') }}
            {{ Form::checkbox('activo', $motivosTrabajo->activo, ['class' => 'form-control' . ($errors->has('activo') ? ' is-invalid' : ''), 'placeholder' => 'Activo']) }}
            {!! $errors->first('activo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
</div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>