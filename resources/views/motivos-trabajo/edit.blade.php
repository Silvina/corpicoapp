@extends('adminlte::page')

@section('title', 'Motivo de trabajo')

@section('content_header')
    <h1>Motivo de Trabajo</h1>
@stop


@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Editar Motivo de Trabajo</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('motivos-trabajo.update', $motivosTrabajo->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('motivos-trabajo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
