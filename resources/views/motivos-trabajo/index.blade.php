@extends('adminlte::page')

@section('title', 'Motivos de Trabajo')

@section('content_header')
<h1>Motivos de Trabajo</h1>
@stop


@section('content')
<div class="container-fluid">
	@if (session('success'))
	<div class="alert alert-success" role="success">
		{{ session('success') }}
	</div>
	@endif
	@if (session('error'))
	<div class="alert alert-error" role="error">
		{{ session('error') }}
	</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">

						<span id="card_title">
							{{ __('Motivos de Trabajo') }}
						</span>

						<div class="float-right">
							<a href="{{ route('motivos-trabajo.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
								{{ __('Create New') }}
							</a>
						</div>
					</div>
				</div>
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
				@endif

				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead class="thead">
								<tr>

									<th>Descripción</th>
									<th>Abrev.</th>
									<th>Dias Resol.</th>
									<th>Alerta Estado Luego Generar</th>
									<th>Estado Luego Generar</th>
									<th>Colocacion</th>
									<th>Retiro</th>
									<th>Informativo</th>
									<th>Precinto Hab</th>
									<th>Precinto Med</th>
									<th>Instalacion</th>
									<th>Materiales</th>
									<th>Permite Eliminar</th>
									<th>Procesa Tomaestado</th>
									<th>Activo</th>

									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($motivosTrabajos as $motivosTrabajo)
								<tr>
									<td>{{ $motivosTrabajo->descripcion }}</td>
									<td>{{ $motivosTrabajo->abreviatura }}</td>
									<td>{{ $motivosTrabajo->dias_resolucion }}</td>
									<td>{{ $motivosTrabajo->alerta_estado_luego_generar }}</td>
									<td>{{ $motivosTrabajo->estado_luego_generar }}</td>
									<td>{{ $motivosTrabajo->solicita_colocacion }}</td>
									<td>{{ $motivosTrabajo->solicita_retiro }}</td>
									<td>{{ $motivosTrabajo->solicita_informativo }}</td>
									<td>{{ $motivosTrabajo->solicita_precinto_hab }}</td>
									<td>{{ $motivosTrabajo->solicita_precinto_med }}</td>
									<td>{{ $motivosTrabajo->solicita_instalacion }}</td>
									<td>{{ $motivosTrabajo->solicita_materiales }}</td>
									<td>{{ $motivosTrabajo->permite_eliminar }}</td>
									<td>{{ $motivosTrabajo->procesa_tomaestado }}</td>

									<td>
										<form action="{{ route('motivos-trabajo.destroy',$motivosTrabajo->id) }}" method="POST">
											<a class="btn btn-sm btn-primary " href="{{ route('motivos-trabajo.show',$motivosTrabajo->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
											<a class="btn btn-sm btn-success" href="{{ route('motivos-trabajo.edit',$motivosTrabajo->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			{!! $motivosTrabajos->links() !!}
		</div>
	</div>
</div>
@endsection