<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use App\Controllers;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\SessionsController;

Route::get('/login', "App\Http\Controllers\SessionsController@create")->name('login.create');
Route::post('/login', "App\Http\Controllers\SessionsController@store")->name('login.store');
Route::post('/logout', [SessionsController::class,'logout']);
Route::get('/', "App\Http\Controllers\SessionsController@create")->name('login.create');

Route::get('home', "App\Http\Controllers\HomeController@index")->name('home');
Route::resource('usuarios', App\Http\Controllers\UsuarioController::class);
Route::resource('cuadrillas', App\Http\Controllers\CuadrillaController::class);
Route::resource('sectores', App\Http\Controllers\SectorController::class);
// Route::resource('tipos-trabajo', App\Http\Controllers\TiposTrabajoController::class);
// Route::resource('motivos-trabajo', App\Http\Controllers\MotivosTrabajoController::class);
Route::resource('asignar-tt-a-cuadrilla', App\Http\Controllers\AsignarTTaCuadrillaController::class);
	//Route::resource('asignar-motivo-a-sector', App\Http\Controllers\AsignarMotivoASectorController::class);

Route::post('asignar-tt-a-cuadrilla_ajax', "App\Http\Controllers\AsignarTTaCuadrillaController@cuadrilla_ajax")->name('cuadrilla_ajax');

//});
